package ereale.uninsubria.disp_mobili.appriuso;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.TreeMap;

public class ConsegneCliente extends AppCompatActivity { //per vedere le consegne specifiche di un cliente
    private static final String TAG = "ConsegneCliente";
    private String nome, cognome, tessera, cid;
    private TreeMap<Integer, ConsegneItem> consegneItems;
    private RecyclerAdapterConsegne mAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consegne);
        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        nome = intent.getStringExtra(ClienteItem.NOME);
        cognome=intent.getStringExtra(ClienteItem.COGNOME);
        tessera=intent.getStringExtra(ClienteItem.TESSERA);
        cid=intent.getStringExtra(ClienteItem.CARTA_ID);
        int ind = intent.getIntExtra("indice",1); //indice cliente

        consegneItems = new TreeMap<>();

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mAdapter = new RecyclerAdapterConsegne(consegneItems);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Query q = database.getReference(ConsegneItem.CONSEGNA).orderByChild(ConsegneItem.CLIENTE).equalTo(ind);
        q.addChildEventListener(new ChildEventListener() {
            @Override
            public synchronized void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getValue()!=null) {
                    getConsegnaItemObject(dataSnapshot,Integer.parseInt(dataSnapshot.getKey()));
                    //getConsegnaItemObject(dataSnapshot, indice++);

                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getValue()!=null) {
                    getConsegnaItemObject(dataSnapshot,Integer.parseInt(dataSnapshot.getKey()));
                    /*String chiave = dataSnapshot.getKey();
                    for(int i=1;i<indice;i++){
                        ConsegneItem c=consegneItems.get(i);
                        if(c.prendiCodice().equals(chiave)){
                            getConsegnaItemObject(dataSnapshot,i);

                        }
                    }*/
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null) {
                    consegneItems.remove(Integer.parseInt(dataSnapshot.getKey()));
                    mAdapter.notifyDataSetChanged();
                    mAdapter.notifyItemRemoved(Integer.parseInt(dataSnapshot.getKey()));
                    mAdapter.updateFullList(consegneItems);

                    /*String chiave = dataSnapshot.getKey();
                    for(int i=1;i<indice;i++){
                        ConsegneItem c=consegneItems.get(i); //null object reference
                        if((c.prendiCodice()).equals(chiave)){
                            consegneItems.remove(i);
                            mAdapter.notifyDataSetChanged();
                            mAdapter.updateFullList(consegneItems);

                            return;
                        }
                    }*/
                }else{
                    Toast.makeText(getApplicationContext(), "consegna non rimossa", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });

        FloatingActionButton fab = findViewById(R.id.fab); //non c'è
        fab.hide();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==android.R
                .id.home){
            Intent i = new Intent();
            i.putExtra(ClienteItem.NOME,nome);
            i.putExtra(ClienteItem.COGNOME,cognome);
            i.putExtra(ClienteItem.TESSERA,tessera);
            i.putExtra(ClienteItem.CARTA_ID,cid);
            setResult(RESULT_OK, i);
            finish();
        }
        return true;
    }

    private void getConsegnaItemObject(DataSnapshot dataSnapshot,final int ind){
        final String cod = dataSnapshot.child(ConsegneItem.CODICE).getValue().toString();
        //final String cod = dataSnapshot.getKey();
        Log.d(TAG,"Chiave "+cod);
        final int codCli = Integer.parseInt(dataSnapshot.child(ConsegneItem.CLIENTE).getValue().toString());
        Log.d(TAG,"Cliente "+codCli);
        final int categoria = Integer.parseInt(dataSnapshot.child(ConsegneItem.CATEGORIA).getValue().toString());
        Log.d(TAG,"Categoria "+categoria);
        final String data;
        data = dataSnapshot.child(ConsegneItem.DATA).getValue().toString();
        Log.d(TAG,"Data "+data);
        final String descrizione = dataSnapshot.child(ConsegneItem.DESCRIZIONE).getValue().toString();
        Log.d(TAG,"Descrizione "+descrizione);
        final String magazzino=dataSnapshot.child(ConsegneItem.MAGAZZINO).getValue().toString();
        Log.d(TAG,"Magazzino "+magazzino);
        final String fascia=dataSnapshot.child(ConsegneItem.FASCIA).getValue().toString();
        Log.d(TAG,"Fascia "+fascia);
        final boolean isMagazzino=magazzino.equals("true");
        Log.d(TAG,"Magazzino "+isMagazzino);
        final String ritirata=dataSnapshot.child(ConsegneItem.RITIRATA).getValue().toString();
        final boolean rit=ritirata.equals("true");
        Log.d(TAG,"Ritirata"+rit);

        final ConsegneItem c=new ConsegneItem(codCli, descrizione, categoria,data,isMagazzino,fascia);


        c.decidiNomeCliente(nome+" "+cognome);
        c.impostaRitirata(rit);
        c.setCodice(cod);
        Log.d("CODICE",cod+" "+nome+" "+cognome);
        consegneItems.put(ind,c);
        mAdapter.notifyDataSetChanged();
        mAdapter.notifyItemInserted(ind);
        mAdapter.updateFullList(consegneItems);
        Log.d(TAG,"Istanza registrata "+c);
        Log.d(TAG,"Istanza registrata "+c.toString());


    }
}