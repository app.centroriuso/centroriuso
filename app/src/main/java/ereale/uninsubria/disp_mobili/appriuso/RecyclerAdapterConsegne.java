package ereale.uninsubria.disp_mobili.appriuso;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.Map;
import java.util.TreeMap;

public class RecyclerAdapterConsegne extends RecyclerView.Adapter<RecyclerAdapterConsegne.ConsegneItemHolder> implements Filterable {

    private TreeMap<Integer,ConsegneItem> mConsegneItems;
    private TreeMap<Integer,ConsegneItem> mConsegneItemsFull;

    public RecyclerAdapterConsegne(TreeMap<Integer, ConsegneItem> items){mConsegneItems = items;}

    public void updateFullList(TreeMap<Integer, ConsegneItem> items){
        mConsegneItemsFull = new TreeMap<>(items);
    }

    // The adapter will work out how many items to display.
    @Override
    public int getItemCount() {
        return mConsegneItems.size();
    }

    // When there are no ViewHolders available. Then RecylerView asks to onCreateViewHolder() to make a new one. The item layout is used to create a view for the ViewHolder.
    @Override
    public RecyclerAdapterConsegne.ConsegneItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_consegne, parent, false);
        return new RecyclerAdapterConsegne.ConsegneItemHolder(inflatedView);
    }

    // Passing in a copy of your ViewHolder and the position where the item will show in your RecyclerView.
    @Override
    public void onBindViewHolder(RecyclerAdapterConsegne.ConsegneItemHolder holder, int position) {
        int c=0, key=mConsegneItems.firstKey();
        for(Map.Entry<Integer,ConsegneItem> entry : mConsegneItems.entrySet()){
            if(c++==position){
                key = entry.getKey();
                break;
            }
        }
        ConsegneItem todoItem = mConsegneItems.get(key);
        holder.bindTodoItem(todoItem);
    }

    public static class ConsegneItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {// 1

        private TextView mItemCod,mItemRitirata, mItemDescrizione, mItemMagazzino;
        private ImageView mItemImg;
        private ConsegneItem mConsegneItem;
        private int idCliente;
        String mItemData,mItemCategoria,mItemFascia,mItemCliente ;

        public ConsegneItemHolder(@NonNull View v) {//3
            super(v);
            mItemCod = v.findViewById(R.id.item_codice);
            mItemDescrizione = v.findViewById(R.id.item_descrizione);
            mItemMagazzino = v.findViewById(R.id.item_magazzino);
            mItemRitirata=v.findViewById(R.id.item_ritirata);
            mItemImg=v.findViewById(R.id.item_imgCategoria);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {//4

            Context context = v.getContext();
            Intent mIntent = new Intent(context, InfoConsNoMod.class);
            mIntent.putExtra("chiamante","ConsegneActivity");
            mIntent.putExtra(ConsegneItem.CODICE,mItemCod.getText().toString());
            mIntent.putExtra(ConsegneItem.DATA,mItemData);
            mIntent.putExtra(ConsegneItem.CLIENTE,mItemCliente);
            mIntent.putExtra(ConsegneItem.IDCLIENTE,idCliente);
            mIntent.putExtra(ConsegneItem.DESCRIZIONE,mItemDescrizione.getText().toString());
            mIntent.putExtra(ConsegneItem.MAGAZZINO,mItemMagazzino.getText().toString());//invia Magazzino/No magazzino
            mIntent.putExtra(ConsegneItem.CATEGORIA,mItemCategoria);
            mIntent.putExtra(ConsegneItem.FASCIA,mItemFascia);
            mIntent.putExtra(ConsegneItem.RITIRATA,mItemRitirata.getText().toString()); //passa Ritirato/Non ritirato
            context.startActivity(mIntent);
        }

        public void bindTodoItem(ConsegneItem item) {// 5
            mConsegneItem = item;
            mItemCod.setText(item.getCodice());
            mItemData=item.getData();
            idCliente=item.getCliente();
            mItemCliente=item.nomeCliente();
            mItemDescrizione.setText(item.getDescrizione());
            mItemCategoria=Categoria.getNome(""+item.getCategoria());
            mItemMagazzino.setText(item.magazzinoString());
            mItemFascia=item.getFascia();
            if(item.isRitirata())
                mItemRitirata.setText("Ritirato");
            else mItemRitirata.setText("Non ritirato");

            Categoria.immagineCategoria(mItemImg,mItemCategoria);
        }


    }

    @Override
    public Filter getFilter() { return consegneItemsFilter; }

    private Filter consegneItemsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            TreeMap<Integer, ConsegneItem> filteredList = new TreeMap<>();
            if(constraint ==null || constraint.length()==0){
                filteredList.putAll(mConsegneItemsFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();
                for(Map.Entry<Integer,ConsegneItem> entry : mConsegneItemsFull.entrySet()) {
                    int key = entry.getKey();
                    ConsegneItem item = entry.getValue();
                    String r="Non ritirato";
                    if(item.isRitirata())
                        r="Ritirato";

                    if(item.getCodice().toLowerCase().contains(filterPattern) || item.nomeCliente().toLowerCase().contains(filterPattern) || item.magazzinoString().toLowerCase().contains(filterPattern) || item.getDescrizione().toLowerCase().contains(filterPattern) || Categoria.getNome(item.getCategoria()+"").toLowerCase().contains(filterPattern) || item.getFascia().toLowerCase().contains(filterPattern) || item.getData().toLowerCase().contains(filterPattern) || r.toLowerCase().contains(filterPattern)){
                        filteredList.put(key, item);
                    }

                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mConsegneItems.clear();
            mConsegneItems.putAll((TreeMap<Integer, ConsegneItem>)results.values);
            notifyDataSetChanged();
        }
    };

}
