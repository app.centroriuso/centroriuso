package ereale.uninsubria.disp_mobili.appriuso;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Statistiche extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener{
    private FirebaseUser mFirebaseUser;
    private DrawerLayout drawer;
    private Intent intent;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.activity_statistiche);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_statistiche1);
        }
        Toolbar toolbar = findViewById(R.id.tool_bar);
        toolbar.setElevation(0);
        setSupportActionBar(toolbar);

        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        //questo è il punto
        if (mFirebaseUser == null) {
            Intent intent = new Intent(this, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        drawer=findViewById(R.id.drawer_layout);
        NavigationView navigationView=findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this,drawer,toolbar,R.string.open,R.string.close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //TextView emailTxt=findViewById(R.id.txtEmail);
        //if(mFirebaseUser!=null)
        //emailTxt.setText(mFirebaseUser.getEmail());

    }
    @Override
    public void onClick(View view) {
        if(view == findViewById(R.id.buttonConsRit) || view==findViewById(R.id.imageButtonConsRit)){
            intent = new Intent(Statistiche.this, GraficoConRit.class);
            startActivity(intent);
        }else if(view == findViewById(R.id.buttonCategorie) || view==findViewById(R.id.imageButtonCategorie)){
            intent = new Intent(Statistiche.this, GraficoCategorie.class);
            startActivity(intent);
        }else if(view == findViewById(R.id.buttonGuadagni) || view==findViewById(R.id.imageButtonGuadagni)){
            intent = new Intent(Statistiche.this, GraficoGuadagni.class);
            startActivity(intent);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu miomenu) {
        getMenuInflater().inflate(R.menu.menu_logout, miomenu);
        return true;
    }

    public void onBackPressed(){
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item){
        switch (item.getItemId()){
            case R.id.consegne:
                Intent intent=new Intent(this,ConsegneActivity.class);
                this.startActivity(intent);
                break;
            case R.id.ritiri:
                Intent intent1=new Intent(this,RitiroActivity.class);
                this.startActivity(intent1);
                break;
            case R.id.home:
                Intent intent2=new Intent(this,MainActivity.class);
                this.startActivity(intent2);
                break;
            case R.id.clienti:
                Intent intent3=new Intent(this,ClienteActivity.class);
                this.startActivity(intent3);
                break;
            case R.id.statistiche:
                Intent intent4=new Intent(this,Statistiche.class);
                this.startActivity(intent4);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.action_logout){
            AlertDialog.Builder alert = new AlertDialog.Builder(Statistiche.this);
            alert.setTitle("Logout");
            alert.setMessage("Sei sicuro di voler effettuare il logout?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FirebaseAuth.getInstance().signOut();
                    mFirebaseUser = null;
                    intent = new Intent(Statistiche.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });
            alert.create().show();
        }
        return true;
    }
}
