package ereale.uninsubria.disp_mobili.appriuso;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class RecyclerAdapterNewConsegne extends RecyclerView.Adapter<RecyclerAdapterNewConsegne.ConsegneItemHolder> {
    public static ArrayList<AdapterConsegneItem> mRitiroItems;
    public RecyclerAdapterNewConsegne(ArrayList<AdapterConsegneItem> items){mRitiroItems = items;}
    // The adapter will work out how many items to display.
    @Override
    public int getItemCount() {return mRitiroItems.size();}
    // When there are no ViewHolders available. Then RecylerView asks to onCreateViewHolder() to make a new one. The item layout is used to create a view for the ViewHolder.
    @Override
    public RecyclerAdapterNewConsegne.ConsegneItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_consegne, parent, false);
        return new RecyclerAdapterNewConsegne.ConsegneItemHolder(inflatedView);
    }
    // Passing in a copy of your ViewHolder and the position where the item will show in your RecyclerView.
    @Override
    public void onBindViewHolder(RecyclerAdapterNewConsegne.ConsegneItemHolder holder, int position) {
        holder.bindTodoItem(mRitiroItems.get(position));
    }
    public static class ConsegneItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {// 1
        // 2
        private TextView mItemDescrizione,mItemFascia, mItemCategoria, mItemMagazzino;
        private ImageView mItemImg;

        public ConsegneItemHolder(View v) {//3
            super(v);
            mItemFascia = v.findViewById(R.id.editTxtFascia);
            mItemDescrizione = v.findViewById(R.id.editTxtDescrizione);
            mItemCategoria = v.findViewById(R.id.editTxtCategoria);
            mItemImg = v.findViewById(R.id.imageViewProdotto);
            mItemMagazzino=v.findViewById(R.id.editTxtMagazzino);
            v.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {// 4 //vuoto perchè non voglio che faccia niente
        }
        public void bindTodoItem(AdapterConsegneItem item) {// 5
            mItemFascia.setText(item.getFascia());
            mItemDescrizione.setText(item.getDescrizione());
            mItemCategoria.setText(item.getCategoria());
            mItemMagazzino.setText(item.getMagazzino());
            Categoria.immagineCategoria(mItemImg, item.getCategoria());
        }
    }
}
