package ereale.uninsubria.disp_mobili.appriuso;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import java.util.Map;
import java.util.TreeMap;

public class RecyclerAdapterClienti extends RecyclerView.Adapter<RecyclerAdapterClienti.ClienteItemHolder> implements Filterable {
    private TreeMap<Integer,ClienteItem> mClienteItems;
    private TreeMap<Integer,ClienteItem> mClienteItemsFull;
    public RecyclerAdapterClienti(TreeMap<Integer, ClienteItem> items){mClienteItems = items;}
    public void updateFullList(TreeMap<Integer, ClienteItem> items){
        mClienteItemsFull = new TreeMap<>(items);
    }
    // The adapter will work out how many items to display.
    @Override
    public int getItemCount() {
        return mClienteItems.size();
    }
    // When there are no ViewHolders available. Then RecylerView asks to onCreateViewHolder() to make a new one. The item layout is used to create a view for the ViewHolder.
    @Override
    public RecyclerAdapterClienti.ClienteItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_cliente, parent, false);
        return new ClienteItemHolder(inflatedView);
    }
    // Passing in a copy of your ViewHolder and the position where the item will show in your RecyclerView.
    @Override
    public void onBindViewHolder(RecyclerAdapterClienti.ClienteItemHolder holder, int position) {
        int c=0, key=mClienteItems.firstKey();
        for(Map.Entry<Integer,ClienteItem> entry : mClienteItems.entrySet()){
            if(c++==position){
                key = entry.getKey();
                break;
            }
        }
        ClienteItem todoItem = mClienteItems.get(key);
        holder.bindTodoItem(todoItem);
    }
    public static class ClienteItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {// 1
        // 2
        private TextView mItemCid,mItemNome,mItemCognome, mItemTessera;
        public ClienteItemHolder(View v) {//3
            super(v);
            mItemCid = v.findViewById(R.id.item_cidentita);
            mItemNome = v.findViewById(R.id.item_nome);
            mItemCognome = v.findViewById(R.id.item_cognome);
            mItemTessera = v.findViewById(R.id.item_tessera);
            v.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {// 4
            Context context = v.getContext();
            Intent mIntent = new Intent(context, InfoCliNoMod.class);
            mIntent.putExtra(ClienteItem.NOME,mItemNome.getText().toString());
            mIntent.putExtra(ClienteItem.COGNOME,mItemCognome.getText().toString());
            mIntent.putExtra(ClienteItem.TESSERA,mItemTessera.getText().toString());
            mIntent.putExtra(ClienteItem.CARTA_ID,mItemCid.getText().toString());
            context.startActivity(mIntent);
        }
        public void bindTodoItem(ClienteItem item) {// 5
            mItemCid.setText(item.getCartaId());
            mItemNome.setText(item.getNome());
            mItemCognome.setText(item.getCognome());
            mItemTessera.setText(item.getTessera());
        }
    }
    @Override
    public Filter getFilter() {
        return clienteItemsFilter;
    }
    private Filter clienteItemsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            TreeMap<Integer, ClienteItem> filteredList = new TreeMap<>();
            if(constraint ==null || constraint.length()==0){
                filteredList.putAll(mClienteItemsFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();
                for(Map.Entry<Integer,ClienteItem> entry : mClienteItemsFull.entrySet()) {
                    int key = entry.getKey();
                    ClienteItem item = entry.getValue();
                    if(item.getCartaId().toLowerCase().contains(filterPattern) || item.getNome().toLowerCase().contains(filterPattern) || item.getCognome().toLowerCase().contains(filterPattern) || item.getTessera().toLowerCase().contains(filterPattern)){
                        filteredList.put(key, item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mClienteItems.clear();
            mClienteItems.putAll((TreeMap<Integer, ClienteItem>)results.values);
            notifyDataSetChanged();
        }
    };
}
