package ereale.uninsubria.disp_mobili.appriuso;

import android.util.Log;

import java.util.Collection;
import java.util.TreeMap;

public class Fascia {
    private static TreeMap<String, FasciaItem> list;
    private static TreeMap<String, FasciaItem> dbList;
    private static FasciaItem b = new FasciaItem("Bianco", 0.5, 5);
    private static FasciaItem g = new FasciaItem("Giallo", 5, 10);
    private static FasciaItem v = new FasciaItem("Verde", 10, 15);
    private static FasciaItem a = new FasciaItem("Azzurro", 15, 20);
    private static FasciaItem r = new FasciaItem("Rosso", 20, 30);
    private static FasciaItem rb = new FasciaItem("Rosso Barrato", 30, 0);
    private static void creaListDB(){
        dbList=new TreeMap<String,FasciaItem>();
        dbList.put(b.getNome(),b);
        dbList.put(g.getNome(),g);
        dbList.put(v.getNome(),v);
        dbList.put(a.getNome(),a);
        dbList.put(r.getNome(),r);
        dbList.put(rb.getNome(),rb);
    }
    public static FasciaItem getfasciaDB(String nome){
        if(dbList==null)
            creaListDB();
        return dbList.get(nome);
    }
    public static Collection<String> creaSpinner(){
        list=new TreeMap<String,FasciaItem>();
        list.put(b.getNomeCompleto(), b);
        list.put(g.getNomeCompleto(),g);
        list.put(v.getNomeCompleto(),v);
        list.put(a.getNomeCompleto(),a);
        list.put(r.getNomeCompleto(),r);
        list.put(rb.getNomeCompleto(),rb);
        return list.keySet();
    }
    public static FasciaItem getFascia(String nome){
        if(list==null)
            creaSpinner();
        return list.get(nome);
    }
}