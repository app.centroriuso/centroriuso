package ereale.uninsubria.disp_mobili.appriuso;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;


public class InfoConsNoMod extends AppCompatActivity {
    private static final String TAG = "InfoConsegneNoMod";

    private String cliente, descrizione, categoria, data, magazzino,fascia,codice,ritirata;
    private TextView txtCliente, txtDescrizione,txtCategoria,txtFascia,txtRitirata, txtCodice, txtMagazzino;
    private TextView txtData;
    private Intent intentOpen;
    int idCliente;
    ImageView imgCategoria;

    public static final int MOD_CON = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.info_consegne_nomod);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.info_consegne_nomod1);
        }
        idCliente=0;

        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        codice=(intent.getStringExtra(ConsegneItem.CODICE));
        txtCodice=findViewById(R.id.editTxtCodice);
        txtCodice.setText(codice);

        cliente = intent.getStringExtra(ConsegneItem.CLIENTE);
        txtCliente = findViewById(R.id.editTxtCliente);
        txtCliente.setText(cliente); //non lo fa

        descrizione = intent.getStringExtra(ConsegneItem.DESCRIZIONE);
        txtDescrizione = findViewById(R.id.editTxtDescrizione);
        txtDescrizione.setText(descrizione);

        data = intent.getStringExtra(ConsegneItem.DATA);
        txtData = findViewById(R.id.editTxtData);
        txtData.setText(data);

        categoria=intent.getStringExtra(ConsegneItem.CATEGORIA);
        txtCategoria= findViewById(R.id.editTxtCategoria);
        txtCategoria.setText(categoria);
        imgCategoria=findViewById(R.id.imageView3);
        Categoria.immagineCategoria(imgCategoria,categoria);

        fascia=intent.getStringExtra(ConsegneItem.FASCIA);
        txtFascia=findViewById(R.id.editTxtFascia);
        txtFascia.setText(fascia);

        ritirata=intent.getStringExtra(ConsegneItem.RITIRATA); //passa Ritirato/Non ritirato
        txtRitirata=findViewById(R.id.editTxtRitiro);
        txtRitirata.setText(ritirata);


        magazzino = intent.getStringExtra(ConsegneItem.MAGAZZINO); //passa Magazzino/No magazino
        txtMagazzino=findViewById(R.id.editTxtMagazzino);
        txtMagazzino.setText(magazzino);

        Button modificaConsegna = findViewById(R.id.buttonSalvaModifiche);
        modificaConsegna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentOpen = new Intent(InfoConsNoMod.this, InfoConsegne.class);
                intentOpen.putExtra(ConsegneItem.CODICE,codice);
                intentOpen.putExtra(ConsegneItem.CLIENTE, txtCliente.getText().toString());
                intentOpen.putExtra(ConsegneItem.DESCRIZIONE, txtDescrizione.getText().toString());
                intentOpen.putExtra(ConsegneItem.DATA, txtData.getText().toString());

                String m="false";
                if((txtMagazzino.getText().toString()).equals("Magazzino"))
                    m="true";

                intentOpen.putExtra(ConsegneItem.MAGAZZINO, m);
                intentOpen.putExtra(ConsegneItem.CATEGORIA,txtCategoria.getText().toString());
                intentOpen.putExtra(ConsegneItem.FASCIA, txtFascia.getText().toString());
                if(txtRitirata.getText().toString().equals("Ritirato"))
                    intentOpen.putExtra(ConsegneItem.RITIRATA,"true");
                else
                    intentOpen.putExtra(ConsegneItem.RITIRATA,"false");

                startActivityForResult(intentOpen, MOD_CON);
            }
        });

        Button eliminaConsegna = findViewById(R.id.buttonElimina);
        eliminaConsegna.setOnClickListener(new View.OnClickListener() {
            @Override
            public synchronized void onClick(View view) {
                if(txtRitirata.getText().toString().equals("Non ritirato")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(InfoConsNoMod.this);
                    alert.setTitle("Eliminazione consegna");
                    alert.setMessage("Sei sicuro di voler eliminare " + txtDescrizione.getText().toString() + " dall'elenco delle consegne?");
                    alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            DatabaseReference myRef1 = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
                            Query query1=myRef1.orderByChild(ConsegneItem.CODICE).equalTo(codice);
                            query1.addChildEventListener(new ChildEventListener() {
                                @Override
                                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                    String codice;
                                    Log.d(TAG,dataSnapshot+"");
                                    codice=dataSnapshot.getKey();
                                    Log.d(TAG,codice);
                                    FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + codice ).removeValue();
                                    finish();
                                }

                                @Override
                                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                                @Override
                                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}

                                @Override
                                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {}
                            });


                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alert.create().show();
                }else{
                    Toast.makeText(getApplicationContext(), "Consegna ritirata, non è eliminabile", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button aggiungiRitiro= findViewById(R.id.buttonAggRitiro);
        aggiungiRitiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txtRitirata.getText().toString().equals("Non ritirato")) {
                    intentOpen = new Intent(InfoConsNoMod.this, NewRitiriActivity.class);
                    intentOpen.putExtra("chiamante","InfoConsNoMod" );
                    intentOpen.putExtra(ConsegneItem.CODICE, codice);
                    intentOpen.putExtra(ConsegneItem.CLIENTE, txtCliente.getText().toString());
                    intentOpen.putExtra(ConsegneItem.DESCRIZIONE, txtDescrizione.getText().toString());
                    intentOpen.putExtra(ConsegneItem.DATA, txtData.getText().toString());
                    RadioButton yes = findViewById(R.id.radioYes);
                    String m = "false";
                    if (txtMagazzino.getText().toString().equals("Magazzino"))
                        m = "true";

                    intentOpen.putExtra(ConsegneItem.MAGAZZINO, m); //passa true/false
                    intentOpen.putExtra(ConsegneItem.CATEGORIA, txtCategoria.getText().toString());
                    intentOpen.putExtra(ConsegneItem.FASCIA, txtFascia.getText().toString());

                    startActivityForResult(intentOpen,1); //da gestire solo per settare il true/false in ritirata
                }else{
                    //Toast.makeText(getApplicationContext(), "consegna gia' ritirata", Toast.LENGTH_LONG).show();
                    AlertDialog.Builder alert = new AlertDialog.Builder(InfoConsNoMod.this);
                    alert.setTitle("Consegna già ritirata");
                    alert.setMessage("Vuoi vedere il ritiro di " + txtDescrizione.getText().toString() + " ?");
                    alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            intentOpen = new Intent(InfoConsNoMod.this, InfoRitiroNoMod.class);
                            intentOpen.putExtra("chiamante","InfoConsNoMod" );
                            intentOpen.putExtra(ConsegneItem.CODICE, codice);
                            intentOpen.putExtra(ConsegneItem.CLIENTE, txtCliente.getText().toString());
                            intentOpen.putExtra(ConsegneItem.DESCRIZIONE, txtDescrizione.getText().toString());
                            intentOpen.putExtra(ConsegneItem.DATA, txtData.getText().toString());
                            RadioButton yes = findViewById(R.id.radioYes);
                            String m = "false";
                            if (txtMagazzino.getText().toString().equals("Magazzino"))
                                m = "true";

                            intentOpen.putExtra(ConsegneItem.MAGAZZINO, m);
                            intentOpen.putExtra(ConsegneItem.CATEGORIA, txtCategoria.getText().toString());
                            intentOpen.putExtra(ConsegneItem.FASCIA, txtFascia.getText().toString());

                            startActivityForResult(intentOpen, 3); //da gestire solo per settare il true/false in ritirata
                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alert.create().show();

                }
            }
        });
    }
    @Override
    protected synchronized void onActivityResult(int requestCode, int resultCode, Intent dati) {
        if(resultCode == RESULT_CANCELED) {
            txtCliente.setText(cliente);
            txtDescrizione.setText(descrizione);
            txtData.setText(data);

            txtMagazzino.setText(magazzino);

            txtCategoria.setText(categoria);
            Categoria.immagineCategoria(imgCategoria,categoria);
            txtFascia.setText(fascia);

            if(ritirata.equals("true"))
                txtRitirata.setText("Ritirato");
            else txtRitirata.setText("Non ritirato");

        }else if(resultCode == RESULT_OK){
            if(requestCode == MOD_CON){ //se ritorna da infoconsegne

            txtCliente.setText(dati.getStringExtra(ConsegneItem.CLIENTE));
            cliente=txtCliente.getText().toString();
            txtDescrizione.setText(dati.getStringExtra(ConsegneItem.DESCRIZIONE));
            descrizione=txtDescrizione.getText().toString();
            txtData.setText(dati.getStringExtra(ConsegneItem.DATA));
            magazzino=dati.getStringExtra(ConsegneItem.MAGAZZINO); //ricevo true/false

            if(magazzino.equals("true"))
                txtMagazzino.setText("Magazzino");
            else
                txtMagazzino.setText("No magazzino");

            txtCategoria.setText(dati.getStringExtra(ConsegneItem.CATEGORIA));
            categoria=txtCategoria.getText().toString();
            Categoria.immagineCategoria(imgCategoria,categoria);
            txtFascia.setText(dati.getStringExtra(ConsegneItem.FASCIA));
            fascia=txtFascia.getText().toString();
            boolean r;
            if(dati.getStringExtra(ConsegneItem.RITIRATA).equals("true")){
                txtRitirata.setText("Ritirato"); r=true;}
            else {txtRitirata.setText("Non ritirato"); r=false;}

                boolean m=false;
                if(magazzino.equals("true"))
                    m=true;

                Log.d(TAG,dati.getStringExtra(ConsegneItem.DESCRIZIONE));
                Log.d(TAG,dati.getStringExtra(ConsegneItem.CATEGORIA));
                Log.d(TAG,dati.getStringExtra(ConsegneItem.DATA));
                Log.d(TAG,""+m);
                Log.d(TAG,dati.getStringExtra(ConsegneItem.FASCIA));

                ConsegneItem returnValue = new ConsegneItem(idCliente, descrizione, Categoria.getID(categoria), txtData.getText().toString(), m, fascia); //devo gestirla come text view forse? //errore qui
                returnValue.impostaRitirata(r);
                daiChiave(cliente,returnValue);

                magazzino=txtMagazzino.getText().toString(); //perchè se no se apro faccio modifico e modifico poi rischiaccio modifica e non modifico in magazzino è presente true perchè qui non lo setto a Magazzino e sopra faccio assegnamento da magazzino


            }else if(requestCode==1){ //quando vado in newRitiriactivity
                if(dati.getStringExtra(ConsegneItem.RITIRATA).equals("true"))
                    txtRitirata.setText("Ritirato");

            }else if(requestCode==3){ //quando vado in inforitirinomod
                txtCliente.setText(cliente);
                txtDescrizione.setText(descrizione);
                txtData.setText(data);
                txtRitirata.setText(dati.getStringExtra(ConsegneItem.RITIRATA));
                txtMagazzino.setText(magazzino);

                txtCategoria.setText(categoria);
                Categoria.immagineCategoria(imgCategoria,categoria);
                txtFascia.setText(fascia);
            }

        }
    }
    private synchronized void aggiungiDB(final ConsegneItem r){
        final String consegna=r.getCodice();
        String[] d=data.split("-");
        String[] t=txtData.getText().toString().split("-");
        Log.d(TAG,"Data anno "+d[2]);
        Log.d(TAG,"Data anno "+t[2]);
        if(!(d[1].equals(t[1])) || !(d[2].equals(t[2]))){ //cambio codice solo se cambio il mese e l'anno non se cambio solo il giorno, perchè in quel caso è ricavabile

            if( txtRitirata.getText().toString().equals("Ritirato")) { //se mi si modifica il codice (modificando la data), devo modificare il codice anche in ritiro

                DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO);
                Query query = myRef.orderByChild(RitiroItem.CONSEGNA).equalTo(codice);
                query.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        String codice;
                        codice = dataSnapshot.getKey();

                        DatabaseReference refCon = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO + "/" + codice + "/" + RitiroItem.CONSEGNA);
                        Log.d(TAG, "Codice consegna " + consegna);
                        refCon.setValue(consegna);
                        Log.d(TAG, refCon + "");

                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }


        }else{
            r.setCodice(this.codice); //se non mi modifica la data voglio che il codice resti lo stesso

        }

        DatabaseReference myRef1 = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        Query query1=myRef1.orderByChild(ConsegneItem.CODICE).equalTo(this.codice);
        query1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String codice;
                Log.d(TAG,dataSnapshot+"");
                codice=dataSnapshot.getKey();
                Log.d(TAG,codice);
                FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + codice).setValue(r);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });

        data=txtData.getText().toString();
        codice=r.getCodice();
        txtCodice.setText(codice);

    }


    private synchronized void daiChiave(final String cliente, final ConsegneItem returnValue){
        final String[] nome=cliente.split(" ");
        Log.d(TAG,"Nome:"+nome[0]+"Cognome:"+nome[1]);
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
        Log.d(TAG,myRef+"");
        Query query = myRef.orderByChild(ClienteItem.COGNOME).equalTo(nome[1]);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    Log.d(TAG,""+d);
                    Log.d(TAG,d.child(ClienteItem.NOME)+"");
                    if ((d.child(ClienteItem.NOME).getValue().toString()).equals(nome[0])) { //se ci sono due persone con lo stesso nome e cognome non funziona (a Malnate c'è gente con stesso nome e cognome
                        returnValue.setCliente(Integer.parseInt(d.getKey()));
                        idCliente = Integer.parseInt(d.getKey());
                        Log.d(TAG,"Cod dentro ciclo "+idCliente);
                        Log.d(TAG,returnValue.getCliente()+"");
                        returnValue.decidiNomeCliente(cliente);
                        aggiungiDB(returnValue);
                        Log.d(TAG,"Cod Cliente registrato "+idCliente);

                        return;
                    }
                    Log.d(TAG,idCliente+"");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}

        });
        Log.d(TAG,"restituisco cod cli "+idCliente);
        Log.d(TAG,"restituisco cod cli "+returnValue.getCliente());
        Log.d(TAG,"restituisco cod cli "+txtCliente.getText().toString());

    }

}