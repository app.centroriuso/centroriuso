package ereale.uninsubria.disp_mobili.appriuso;

import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.TreeMap;

public class RitiroItem {
    private String nome;
    private String consegna;
    private String data;
    private int cliente;
    private double offerta;
    private int id;

    static final String RITIRO = "ritiro";
    static final String CONSEGNA ="consegna";
    static final String DATA ="data";
    static final String CLIENTE = "cliente";
    static final String OFFERTA = "offerta";
    static final String ID ="id";
    static final String IDCLIENTE="idCliente";

    public RitiroItem() {}
    public RitiroItem(String consegna, String data, int cliente, double offerta) {
        this.consegna = consegna;
        this.data = data;
        this.cliente = cliente;
        this.offerta = offerta;
    }

    public int prendiID(){return id;}
    public void impostaID(int i){id=i;}

    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public int getCliente() {
        return cliente;
    }

    public String getConsegna() {
        return consegna;
    }

    public void setConsegna(String consegna) {
        this.consegna = consegna;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void decidiNomeCliente(String n){nome = n;}

    public String nomeCliente() {return nome;}

    public double getOfferta() {return offerta;}

    public void setOfferta(double offerta) {this.offerta = offerta;}
}
