package ereale.uninsubria.disp_mobili.appriuso;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class InfoCliNoMod extends AppCompatActivity {
    private String nome, cognome, tessera, cartaId;
    private TextView txtCid, txtNome, txtCognome, txtTessera, txtNCons, txtNRit;
    private Intent intentOpen;
    private int indice;
    public static final int MOD_CLI = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.info_cliente_nomod);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.info_cliente_nomod1);
        }

        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        nome = intent.getStringExtra(ClienteItem.NOME);
        txtNome = findViewById(R.id.editTxtNome);
        txtNome.setText(nome);
        cognome = intent.getStringExtra(ClienteItem.COGNOME);
        txtCognome = findViewById(R.id.editTxtCognome);
        txtCognome.setText(cognome);
        tessera = intent.getStringExtra(ClienteItem.TESSERA);
        txtTessera = findViewById(R.id.editTxtTessera);
        txtTessera.setText(tessera);
        cartaId = intent.getStringExtra(ClienteItem.CARTA_ID);
        txtCid = findViewById(R.id.editTxtCId);
        txtCid.setText(cartaId);
        txtNCons=findViewById(R.id.editTxtNCons);
        txtNRit=findViewById(R.id.editTxtNRit);
        final Button eliminaCliente = findViewById(R.id.buttonElimina);
        Button modificaCliente = findViewById(R.id.buttonModifica);

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
        Query query = myRef.orderByChild(ClienteItem.CARTA_ID).equalTo(txtCid.getText().toString());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()) {
                    for (DataSnapshot data : dataSnapshot.getChildren())
                        indice = Integer.parseInt(data.getKey());
                    DatabaseReference myRefCons = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
                    Query queryC = myRefCons.orderByChild(ConsegneItem.CLIENTE).equalTo(indice);
                    queryC.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            txtNCons.setText(String.valueOf(dataSnapshot.getChildrenCount())); //numero di istanze
                            if(dataSnapshot.getChildrenCount()>0)
                                eliminaCliente.setEnabled(false);
                        }
                        @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                    });
                    DatabaseReference myRefRit = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO);
                    Query queryR = myRefRit.orderByChild(RitiroItem.CLIENTE).equalTo(indice);
                    queryR.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            txtNRit.setText(String.valueOf(dataSnapshot.getChildrenCount()));
                            if(dataSnapshot.getChildrenCount()>0) //conta istanze
                                eliminaCliente.setEnabled(false);
                        }
                        @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                    });
                }

            }
            @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
        });

        modificaCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentOpen = new Intent(InfoCliNoMod.this, InfoCli.class);
                intentOpen.putExtra(ClienteItem.NOME,txtNome.getText().toString());
                intentOpen.putExtra(ClienteItem.COGNOME,txtCognome.getText().toString());
                intentOpen.putExtra(ClienteItem.TESSERA,txtTessera.getText().toString());
                intentOpen.putExtra(ClienteItem.CARTA_ID,txtCid.getText().toString());
                startActivityForResult(intentOpen, MOD_CLI);
            }
        });
        eliminaCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(InfoCliNoMod.this);
                alert.setTitle("Eliminazione cliente");
                alert.setMessage("Sei sicuro di voler eliminare "+txtNome.getText().toString()+" "+txtCognome.getText().toString()+" dall'elenco dei clienti?");
                alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE+"/"+indice).removeValue();
                        Toast.makeText(getApplicationContext(), "cliente rimosso", Toast.LENGTH_LONG).show();
                        finish();
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });
                alert.create().show();
                    }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_CANCELED) {
            txtCid.setText(cartaId);
            txtNome.setText(nome);
            txtCognome.setText(cognome);
            txtTessera.setText(tessera);
        }else if(resultCode == RESULT_OK){
            txtCid.setText(data.getStringExtra(ClienteItem.CARTA_ID));
            txtNome.setText(data.getStringExtra(ClienteItem.NOME));
            txtCognome.setText(data.getStringExtra(ClienteItem.COGNOME));
            txtTessera.setText(data.getStringExtra(ClienteItem.TESSERA));
            if(requestCode == MOD_CLI){
                ClienteItem returnValue = new ClienteItem(data.getStringExtra(ClienteItem.CARTA_ID), data.getStringExtra(ClienteItem.NOME), data.getStringExtra(ClienteItem.COGNOME), data.getStringExtra(ClienteItem.TESSERA));
                aggiungiDB(returnValue);
            }
        }
    }
    private synchronized void aggiungiDB(ClienteItem r){
        FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE + "/" + indice).setValue(r);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu miomenu) {
        getMenuInflater().inflate(R.menu.menu_cons_rit, miomenu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }else if(id == R.id.buttonConsegne){
            String c = txtNCons.getText().toString();
            if(c.equals("0"))
                Toast.makeText(getApplicationContext(), nome+" "+cognome+" non ha fatto consegne", Toast.LENGTH_LONG).show();
            else {
                Intent intent = new Intent(this, ConsegneCliente.class);
                intent.putExtra("indice", indice);
                intent.putExtra(ClienteItem.NOME,txtNome.getText().toString());
                intent.putExtra(ClienteItem.COGNOME,txtCognome.getText().toString());
                intent.putExtra(ClienteItem.TESSERA,txtTessera.getText().toString());
                intent.putExtra(ClienteItem.CARTA_ID,txtCid.getText().toString());
                startActivityForResult(intent,1);
            }
        }else if(id==R.id.buttonRitiri){
            String r = txtNRit.getText().toString();
            if(r.equals("0"))
                Toast.makeText(getApplicationContext(), nome+" "+cognome+" non ha fatto ritiri", Toast.LENGTH_LONG).show();
            else{
                Intent intent = new Intent(this, RitiroCliente.class);
                intent.putExtra("indice", indice);
                intent.putExtra(ClienteItem.NOME,txtNome.getText().toString());
                intent.putExtra(ClienteItem.COGNOME,txtCognome.getText().toString());
                intent.putExtra(ClienteItem.TESSERA,txtTessera.getText().toString());
                intent.putExtra(ClienteItem.CARTA_ID,txtCid.getText().toString());
                startActivityForResult(intent,1);
            }
        }
        return true;
    }
}
