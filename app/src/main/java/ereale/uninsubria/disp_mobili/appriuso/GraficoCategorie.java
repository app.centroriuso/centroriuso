package ereale.uninsubria.disp_mobili.appriuso;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

import static ereale.uninsubria.disp_mobili.appriuso.NetworkStateChangeReceiver.IS_NETWORK_AVAILABLE;


public class GraficoCategorie extends AppCompatActivity {
    private static final String RIPRISTINO = "consegne";
    private BarChart chart;
    private TreeMap<Integer, Integer> categorieConsegnateRitirate;
    private TreeMap<String, Integer> consegneRitiri;
    private Button buttonConsegne, buttonRitiri, buttonLegenda;
    private String[] months;
    private int meseCorrente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.grafico_categorie);
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.grafico_categorie1);
        }
        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(!isConnected(GraficoCategorie.this)) {
            final Snackbar snackBar = Snackbar.make(findViewById(R.id.layoutGrafico), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE);
            snackBar.setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GraficoCategorie.this, Statistiche.class);
                    startActivity(intent);
                }
            });
            snackBar.show();
        }
        IntentFilter intentFilter = new IntentFilter(NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                if(!isNetworkAvailable) {
                    final Snackbar snackBar = Snackbar.make(findViewById(R.id.layoutGrafico), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE);
                    snackBar.setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(GraficoCategorie.this, Statistiche.class);
                            startActivity(intent);
                        }
                    });
                    snackBar.show();
                }
            }
        }, intentFilter);

        chart = findViewById(R.id.barchart);

        TextView txtMese = findViewById(R.id.spinnerMese);
        DateFormatSymbols dfs = new DateFormatSymbols();
        months = dfs.getMonths();
        meseCorrente = Calendar.getInstance().get(Calendar.MONTH);
        String mese;
        if (meseCorrente == 0)
            mese=months[11]+" + "+months[meseCorrente];
        else
            mese=months[meseCorrente - 1]+" + "+months[meseCorrente];
        txtMese.setText(mese);
        buttonRitiri = findViewById(R.id.buttonRitiri);
        buttonConsegne = findViewById(R.id.buttonConsegne);
        buttonLegenda = findViewById(R.id.buttonLegenda);
        try{
            String cons = savedInstanceState.getString(RIPRISTINO);
            if(cons.equals("c")) {
                buttonConsegne.setEnabled(false);
                buttonRitiri.setEnabled(true);
            }else{
                buttonConsegne.setEnabled(true);
                buttonRitiri.setEnabled(false);
            }
        }catch(Exception e){
            buttonConsegne.setEnabled(false);
            buttonRitiri.setEnabled(true);
        }
        if (buttonConsegne.isEnabled())
            caricaMeseRit();
        else
            caricaMeseCons();
        buttonRitiri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                caricaMeseRit();
                buttonConsegne.setEnabled(true);
                buttonRitiri.setEnabled(false);
            }
        });
        buttonConsegne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                caricaMeseCons();
                buttonConsegne.setEnabled(false);
                buttonRitiri.setEnabled(true);
            }
        });
        buttonLegenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),LegendaActivity.class);
                startActivity(i);
            }
        });
    }

    private void caricaMeseRit() {
        consegneRitiri = new TreeMap<>();
        categorieConsegnateRitirate = new TreeMap<>();
        for (int i = 1; i < 13; i++)
            categorieConsegnateRitirate.put(i, 0);
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    int annoCtrl,meseP;
                    if(meseCorrente==0){
                        meseP=12;
                        annoCtrl=Calendar.getInstance().get(Calendar.YEAR)-1;
                    }else{
                        annoCtrl=Calendar.getInstance().get(Calendar.YEAR);
                        meseP=meseCorrente;
                    }
                    for(DataSnapshot d: dataSnapshot.getChildren()){
                        StringTokenizer data=new StringTokenizer(d.child(ConsegneItem.DATA).getValue().toString(), "-");
                        data.nextToken();
                        int mese=Integer.parseInt(data.nextToken());
                        int anno=Integer.parseInt(data.nextToken());
                        if((meseP==mese && anno==annoCtrl)|| (meseCorrente+1==mese && anno==Calendar.getInstance().get(Calendar.YEAR))){

                    /*int meseP = (meseCorrente==0? 12 : meseCorrente);
                    for (DataSnapshot d : dataSnapshot.getChildren()) {
                        StringTokenizer data = new StringTokenizer(d.child(ConsegneItem.DATA).getValue().toString(), "-");
                        data.nextToken();
                        int mese = Integer.parseInt(data.nextToken());
                        int anno = Integer.parseInt(data.nextToken());
                        if ((meseP == mese || meseCorrente+1==mese) && anno == Calendar.getInstance().get(Calendar.YEAR)) {*/
                            String c = d.child(RitiroItem.CONSEGNA).getValue().toString();
                            if (consegneRitiri.containsKey(c)) {
                                int n = consegneRitiri.get(c);
                                consegneRitiri.put(c, ++n);
                            } else {
                                consegneRitiri.put(d.child(RitiroItem.CONSEGNA).getValue().toString(), 1);
                            }
                        }
                    }
                    if (consegneRitiri.isEmpty()) {
                        Toast.makeText(getApplicationContext(), "Non ci sono stati ritiri", Toast.LENGTH_LONG).show();
                        chart.clear();
                    } else {
                        DatabaseReference myRefCon = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
                        myRefCon.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null) {
                                    for (DataSnapshot data : dataSnapshot.getChildren()) {
                                        if (consegneRitiri.containsKey(data.child(ConsegneItem.CODICE).getValue().toString())) {
                                            int cat = Integer.parseInt(data.child(ConsegneItem.CATEGORIA).getValue().toString());
                                            int v = categorieConsegnateRitirate.get(cat);
                                            categorieConsegnateRitirate.put(cat, ++v);
                                        }
                                    }
                                    ArrayList ca = new ArrayList();
                                    ArrayList co = new ArrayList();
                                    for (Map.Entry<Integer, Integer> entry : categorieConsegnateRitirate.entrySet()) {
                                        int key = entry.getKey();
                                        int value = entry.getValue();
                                        ca.add(String.valueOf(key));
                                        co.add(new BarEntry(value, --key));
                                    }
                                    creaIstogramma(ca, co, false);
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void caricaMeseCons() {
        categorieConsegnateRitirate = new TreeMap<>();
        for (int i = 1; i < 13; i++)
            categorieConsegnateRitirate.put(i, 0);
        DatabaseReference myRefCon = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        myRefCon.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    int meseP = (meseCorrente==0? 12 : meseCorrente);
                    boolean trovata = false;
                    for (DataSnapshot d : dataSnapshot.getChildren()) {
                        StringTokenizer data = new StringTokenizer(d.child(ConsegneItem.DATA).getValue().toString(), "-");
                        data.nextToken();
                        int mese = Integer.parseInt(data.nextToken());
                        int anno = Integer.parseInt(data.nextToken());
                        int cat = Integer.parseInt(d.child(ConsegneItem.CATEGORIA).getValue().toString());
                        if ((meseP == mese || meseCorrente+1==mese) && anno == Calendar.getInstance().get(Calendar.YEAR)) {
                            int v = categorieConsegnateRitirate.get(cat);
                            categorieConsegnateRitirate.put(cat, ++v);
                            if (!trovata) trovata = true;
                        }
                    }
                    if (trovata) {
                        ArrayList ca = new ArrayList();
                        ArrayList co = new ArrayList();
                        for (Map.Entry<Integer, Integer> entry : categorieConsegnateRitirate.entrySet()) {
                            Log.d("AAA","riempio");
                            int key = entry.getKey();
                            int value = entry.getValue();
                            ca.add(String.valueOf(key));
                            co.add(new BarEntry(value, --key));
                        }
                        creaIstogramma(ca, co, true);
                    } else {
                        Toast.makeText(getApplicationContext(), "Non ci sono state consegne", Toast.LENGTH_LONG).show();
                        chart.clear();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void creaIstogramma(ArrayList categorie, ArrayList consegne, boolean consegnate) {
        BarDataSet bardataset;
        if (consegnate)
            bardataset = new BarDataSet(consegne, "Consegne");
        else
            bardataset = new BarDataSet(consegne, "Ritiri");
        chart.setDrawValueAboveBar(false);
        chart.setDescription("");
        chart.animateY(2000);
        chart.getXAxis().setAxisLineColor(this.getResources().getColor(R.color.colorGrafico));
        chart.getXAxis().setAxisLineWidth(1.3f);
        chart.getXAxis().setTextColor(this.getResources().getColor(R.color.colorGrafico));
        chart.getAxisLeft().setTextColor(this.getResources().getColor(R.color.colorGrafico));
        chart.getAxisLeft().setAxisLineColor(this.getResources().getColor(R.color.colorGrafico));
        chart.getAxisLeft().setAxisLineWidth(2f);
        chart.getAxisLeft().setAxisMinValue(0f);
        chart.getAxisRight().setTextColor(this.getResources().getColor(R.color.colorGrafico));
        chart.getAxisRight().setAxisLineColor(this.getResources().getColor(R.color.colorGrafico));
        chart.getAxisRight().setAxisLineWidth(2f);
        chart.getAxisRight().setAxisMinValue(0f);
        BarData data = new BarData(categorie, bardataset);
        if (consegnate)
            bardataset.setColor(this.getResources().getColor(R.color.colorConsegne));
        else
            bardataset.setColor(this.getResources().getColor(R.color.colorRitiri));
        data.setDrawValues(false);
        chart.setData(data);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (buttonConsegne.isEnabled())
            outState.putString(RIPRISTINO,"m");
        else
            outState.putString(RIPRISTINO, "c");
    }
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else
            return false;
    }
}