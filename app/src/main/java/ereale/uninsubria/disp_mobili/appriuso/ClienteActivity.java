package ereale.uninsubria.disp_mobili.appriuso;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v7.widget.SearchView;
import android.view.inputmethod.EditorInfo;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import java.util.TreeMap;

import static ereale.uninsubria.disp_mobili.appriuso.NetworkStateChangeReceiver.IS_NETWORK_AVAILABLE;

public class ClienteActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private Intent mIntent;
    private RecyclerAdapterClienti mAdapter;
    private TreeMap<Integer, ClienteItem> clienteItems;
    private DrawerLayout drawer;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);
        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        if(!isConnected(ClienteActivity.this)) {
            final Snackbar snackBar = Snackbar.make(findViewById(R.id.drawer_layout), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE);
            snackBar.setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ClienteActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
            snackBar.show();
        }
        IntentFilter intentFilter = new IntentFilter(NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                if(!isNetworkAvailable) {
                    final Snackbar snackBar = Snackbar.make(findViewById(R.id.drawer_layout), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE);
                    snackBar.setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ClienteActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    });
                    snackBar.show();
                }
            }
        }, intentFilter);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();


        //questo è il punto
        if (mFirebaseUser == null) {
            Intent intent = new Intent(this, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        drawer=findViewById(R.id.drawer_layout);
        NavigationView navigationView=findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this,drawer,toolbar,R.string.open,R.string.close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        //Riferimento db
        FirebaseDatabase database = FirebaseDatabase.getInstance();

        NewCliente.riempiAlberi();

        //the array list containing the Cliente items
        clienteItems = new TreeMap<>();

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mAdapter = new RecyclerAdapterClienti(clienteItems);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        database.getReference(ClienteItem.CLIENTE).addChildEventListener(clienteListener);

        // mRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        FloatingActionButton fab = findViewById(R.id.fab);
        mIntent = new Intent(this, NewCliente.class);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIntent.putExtra("chiamante", "ClienteActivity");
                startActivity(mIntent);
            }
        });
    }
    private ChildEventListener clienteListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot.getValue() != null) {
                String cid = dataSnapshot.child(ClienteItem.CARTA_ID).getValue().toString();
                String nome = dataSnapshot.child(ClienteItem.NOME).getValue().toString();
                String cognome = dataSnapshot.child(ClienteItem.COGNOME).getValue().toString();
                String tessera = dataSnapshot.child(ClienteItem.TESSERA).getValue().toString();
                int indice = Integer.parseInt(dataSnapshot.getKey());
                clienteItems.put(indice, new ClienteItem(cid, nome,cognome, tessera));
                mAdapter.notifyItemInserted(clienteItems.size());
                mAdapter.updateFullList(clienteItems);
            }
        }
        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            if (dataSnapshot.getValue() != null) {
                String cid = dataSnapshot.child(ClienteItem.CARTA_ID).getValue().toString();
                String nome = dataSnapshot.child(ClienteItem.NOME).getValue().toString();
                String cognome = dataSnapshot.child(ClienteItem.COGNOME).getValue().toString();
                String tessera = dataSnapshot.child(ClienteItem.TESSERA).getValue().toString();
                int indice = Integer.parseInt(dataSnapshot.getKey());
                clienteItems.put(indice, new ClienteItem(cid, nome, cognome, tessera));
                mAdapter.notifyDataSetChanged();
                mAdapter.updateFullList(clienteItems);
            }
        }
        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            if (dataSnapshot.getValue() != null) {
                int indice = Integer.parseInt(dataSnapshot.getKey());
                clienteItems.remove(indice);
                mAdapter.notifyDataSetChanged();
                mAdapter.updateFullList(clienteItems);
            }
        }
        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };
    @Override
    public boolean onCreateOptionsMenu(Menu miomenu) {
        getMenuInflater().inflate(R.menu.menu, miomenu);

        MenuItem searchItem = miomenu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.action_logout){
            AlertDialog.Builder alert = new AlertDialog.Builder(ClienteActivity.this);
            alert.setTitle("Logout");
            alert.setMessage("Sei sicuro di voler effettuare il logout?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FirebaseAuth.getInstance().signOut();
                    mFirebaseUser = null;
                    Intent intent = new Intent(ClienteActivity.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });
            alert.create().show();
        }
        return true;
    }

    @Override
    public void onBackPressed(){
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item){
        switch (item.getItemId()){
            case R.id.consegne:
                Intent intent=new Intent(this,ConsegneActivity.class);
                this.startActivity(intent);
                break;
            case R.id.ritiri:
                Intent intent1=new Intent(this,RitiroActivity.class);
                this.startActivity(intent1);
                break;
            case R.id.home:
                Intent intent2=new Intent(this,MainActivity.class);
                this.startActivity(intent2);
                break;
            case R.id.clienti:
                Intent intent3=new Intent(this,ClienteActivity.class);
                this.startActivity(intent3);
                break;
            case R.id.statistiche:
                Intent intent4=new Intent(this,Statistiche.class);
                this.startActivity(intent4);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else
            return false;
    }
}
