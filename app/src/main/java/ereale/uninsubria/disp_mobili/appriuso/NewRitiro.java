package ereale.uninsubria.disp_mobili.appriuso;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

public class NewRitiro extends AppCompatActivity {
    private Spinner consegnaSpinner;
    private TreeMap<String, AdapterRitiroItem> consegne;
    private static TreeSet<Integer> indici;
    private ArrayAdapter<String> spinnerAdapterCons;
    private FasciaItem fasciaAttuale;
    private EditText TxtOfferta;
    private TextView TxtDescrizione;
    private ImageView ImgCategoria;
    private double vInserito = 0.0;
    private RitiroItem newRitiro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.new_ritiro);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.new_ritiro1);
        }
        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        aggiornaIDritiri();
        newRitiro = new RitiroItem();
        Intent intent = getIntent();

        /**PER CHIAMATA DA InfoConsNoMod*/
        String consegnaChiamata = intent.getStringExtra(RitiroItem.CONSEGNA);
        consegnaSpinner=(Spinner) findViewById(R.id.spinnerConsegna);
        spinnerAdapterCons= new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item);
        leggiConsegne(consegnaChiamata);

        newRitiro.setCliente(Integer.parseInt(intent.getStringExtra(RitiroItem.CLIENTE)));
        newRitiro.decidiNomeCliente(intent.getStringExtra("NOME"+RitiroItem.CLIENTE));
        newRitiro.setData(intent.getStringExtra(RitiroItem.DATA));
        fasciaAttuale = new FasciaItem();
        TxtOfferta = findViewById(R.id.txtOfferta);
        TxtDescrizione = findViewById(R.id.editTxtDescrizione);
        ImgCategoria = findViewById(R.id.imageViewProdotto);

        Button aggiungiRitiro = findViewById(R.id.buttonAggiungiRitiro);
        aggiungiRitiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ("".equals(TxtOfferta.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Inserire l'offerta", Toast.LENGTH_LONG).show();
                }else {
                    newRitiro.setConsegna(consegnaSpinner.getSelectedItem().toString());
                    fasciaAttuale = Fascia.getFascia(TxtOfferta.getHint().toString());
                    try {
                        String val = TxtOfferta.getText().toString();
                        vInserito = Double.parseDouble(val);
                        if (!(vInserito >= fasciaAttuale.getMinimo() && (vInserito <= fasciaAttuale.getMassimo() || fasciaAttuale.getMassimo() == 0))) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(NewRitiro.this);
                            alert.setTitle("Vuoi procedere col salvataggio?");
                            alert.setMessage("Il prodotto appartiene alla fascia: " + fasciaAttuale.getNomeCompleto());
                            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    newRitiro.setOfferta(vInserito);
                                    AdapterRitiroItem ad = new AdapterRitiroItem();
                                    ad.setConsegna(newRitiro.getConsegna());
                                    ad.setOfferta(String.valueOf(newRitiro.getOfferta()));
                                    ad.setDescrizione(TxtDescrizione.getText().toString());
                                    String c = newRitiro.getConsegna();
                                    ad.setCategoria(consegne.get(c).getCategoria());
                                    ad.setCliente(consegne.get(c).getCliente());
                                    ultimoControlloAddRitiro(newRitiro,ad);
                                }
                            });
                            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override public void onClick(DialogInterface dialog, int which) {}
                            });
                            alert.create().show();
                        } else {
                            newRitiro.setOfferta(vInserito);
                            AdapterRitiroItem ad = new AdapterRitiroItem();
                            ad.setConsegna(newRitiro.getConsegna());
                            ad.setOfferta(String.valueOf(newRitiro.getOfferta()));
                            ad.setDescrizione(TxtDescrizione.getText().toString());
                            String c = newRitiro.getConsegna();
                            ad.setCategoria(consegne.get(c).getCategoria());
                            ad.setCliente(consegne.get(c).getCliente());
                            ultimoControlloAddRitiro(newRitiro,ad);
                        }
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), "L'offerta deve essere un valore numerico", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
    private void ultimoControlloAddRitiro(final RitiroItem r,final AdapterRitiroItem ad){
        if(r.getCliente()==ad.getCliente()){
            AlertDialog.Builder alert = new AlertDialog.Builder(NewRitiro.this);
            alert.setTitle("Sei sicuro di comprare?");
            String message=r.nomeCliente()+" ha effettuato la consegna: "+consegnaSpinner.getSelectedItem().toString();
            alert.setMessage(message);
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    aggiungiRitiro(r,ad);
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override public void onClick(DialogInterface dialog, int which) {}
            });
            alert.create().show();
        }else
            aggiungiRitiro(r,ad);
    }
    private synchronized void aggiungiRitiro(RitiroItem r,AdapterRitiroItem ad){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        int i;
        if(indici == null || indici.size()==0)
            i = 1;
        else
            i = indici.last()+1;
        DatabaseReference myRef = database.getReference(RitiroItem.RITIRO + "/" + i);
        myRef.setValue(r);
        DatabaseReference reference=FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        Query query=reference.orderByChild(ConsegneItem.CODICE).equalTo(r.getConsegna());
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String codice=dataSnapshot.getKey();
                DatabaseReference refCon = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + codice + "/" + ConsegneItem.RITIRATA);
                refCon.setValue(true);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        Toast.makeText(getApplicationContext(), "Nuovo ritiro inserito", Toast.LENGTH_LONG).show();
        Intent intent = new Intent();
        intent.putExtra(RitiroItem.CONSEGNA,r.getConsegna());
        intent.putExtra(RitiroItem.OFFERTA,String.valueOf(r.getOfferta()));
        intent.putExtra(ConsegneItem.CATEGORIA, ad.getCategoria());
        intent.putExtra(ConsegneItem.DESCRIZIONE, ad.getDescrizione());
        intent.putExtra(RitiroItem.RITIRO, String.valueOf(i));
        intent.putExtra(RitiroItem.CLIENTE, String.valueOf(ad.getCliente()));
        setResult(RESULT_OK, intent);
        finish();
    }
    private void leggiConsegne(final String cons){
        consegne = new TreeMap<>();
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        Query query = myRef.orderByChild(ConsegneItem.RITIRATA).equalTo(false);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot data : dataSnapshot.getChildren()){
                    AdapterRitiroItem app = new AdapterRitiroItem();
                    app.setDescrizione(data.child(ConsegneItem.DESCRIZIONE).getValue().toString());
                    app.setCategoria(data.child(ConsegneItem.CATEGORIA).getValue().toString());
                    app.setOfferta(data.child(ConsegneItem.FASCIA).getValue().toString());
                    app.setCliente(Integer.parseInt(data.child(ConsegneItem.CLIENTE).getValue().toString()));
                    consegne.put(data.child(ConsegneItem.CODICE).getValue().toString(), app);
                    spinnerAdapterCons.add(data.child(ConsegneItem.CODICE).getValue().toString());
                }
                consegnaSpinner.setAdapter(spinnerAdapterCons);
                if(cons!=null) {
                    consegnaSpinner.setSelection(spinnerAdapterCons.getPosition(cons));
                }
                consegnaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        //Elemento selezionato: parent.getItemAtPosition(position).toString()
                        String consegna = parent.getItemAtPosition(position).toString();
                        TxtDescrizione.setText(consegne.get(consegna).getDescrizione());
                        Categoria.immagineCategoria(ImgCategoria,consegne.get(consegna).getCategoria());
                        fasciaAttuale = Fascia.getfasciaDB(consegne.get(consegna).getOfferta());
                        TxtOfferta.setHint(fasciaAttuale.getNomeCompleto());
                    }
                    @Override public void onNothingSelected(AdapterView<?> arg0) {}
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        /*query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    Log.d("AAAAAA","ADDES");
                    AdapterRitiroItem app = new AdapterRitiroItem();
                    app.setDescrizione(dataSnapshot.child(ConsegneItem.DESCRIZIONE).getValue().toString());
                    app.setCategoria(dataSnapshot.child(ConsegneItem.CATEGORIA).getValue().toString());
                    app.setOfferta(dataSnapshot.child(ConsegneItem.FASCIA).getValue().toString());
                    consegne.put(dataSnapshot.getKey().replace(";", "/"), app);
                    spinnerAdapterCons.add(dataSnapshot.getKey().replace(";", "/"));
                    if(cons!=null && cons.equals(dataSnapshot.getKey()))
                        consegnaSpinner.setSelection(spinnerAdapterCons.getPosition(dataSnapshot.getKey().replace(";", "/")));
                }
            }
            @Override public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
            @Override public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override public void onCancelled(DatabaseError databaseError) {}
        });*/
    }
    public void aggiornaIDritiri(){
        indici = new TreeSet<>();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Query lastItem = database.getReference().child(RitiroItem.RITIRO).orderByKey().limitToLast(1);
        lastItem.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                indici.add(Integer.parseInt(dataSnapshot.getKey()));
            }
            @Override public void onChildChanged(DataSnapshot dataSnapshot, String s) { }
            @Override public void onChildRemoved(DataSnapshot dataSnapshot) { }
            @Override public void onChildMoved(DataSnapshot dataSnapshot, String s) { }
            @Override public void onCancelled(DatabaseError databaseError) { }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        setResult(RESULT_CANCELED);
        finish();
        return true;
    }
    @Override public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
