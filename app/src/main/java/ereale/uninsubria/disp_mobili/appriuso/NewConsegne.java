package ereale.uninsubria.disp_mobili.appriuso;

import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.FocusFinder;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.time.Instant;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.TreeMap;
import java.util.TreeSet;


public class NewConsegne extends AppCompatActivity {
    private static final String TAG = "NewConsegne";

    int a;
    Spinner categoriaSpinner, fasciaSpinner;
    EditText TxtDescrizione;
    ImageView ImgCategoria;
    ConsegneItem newConsegna;
    RadioGroup radio_group;
    String magazzino,cliente;
    FasciaItem fasciaAttuale;
    private static TreeSet<Integer> indici;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.new_consegna);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.new_consegna1);
        }

        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        aggiornaIDconsegne(); //per determinare l'ultimo id delle consegne utilizzato per crearne uno ad incremento

        newConsegna = new ConsegneItem();
        Intent intent = getIntent();

        cliente=intent.getStringExtra(ConsegneItem.CLIENTE);
        newConsegna.setCliente(Integer.parseInt(intent.getStringExtra(ConsegneItem.IDCLIENTE)));
        newConsegna.setData(intent.getStringExtra(ConsegneItem.DATA));
        TxtDescrizione = findViewById(R.id.editTxtDescrizione);
        ImgCategoria = findViewById(R.id.imageViewProdotto);
        radio_group=findViewById(R.id.radioGroupMagazzino);

        categoriaSpinner=(Spinner) findViewById(R.id.spinnerCategoria);

        ArrayAdapter<String> spinnerAdapter= new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.elenco_categoria));
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item); //ma potrebbe essere superfluo?
        categoriaSpinner.setAdapter(spinnerAdapter);

        fasciaSpinner=(Spinner)findViewById(R.id.spinnerFascia);

        ArrayAdapter<String> spinnerAdapterFascia= new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.elenco_fasce));
        spinnerAdapterFascia.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item); //ma potrebbe essere superfluo?
        fasciaSpinner.setAdapter(spinnerAdapterFascia);


        Button aggiungiConsegna = findViewById(R.id.buttonAggiungiConsegna);
        aggiungiConsegna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String descrizione=TxtDescrizione.getText().toString();
                String categoria = categoriaSpinner.getSelectedItem().toString();
                String fascia=fasciaSpinner.getSelectedItem().toString();
                Log.d(TAG,fascia);
                Log.d(TAG,categoria);
                if(!descrizione.equals("")&& !categoria.equals("") && !fascia.equals("")) { //non controllo magazzino perchè di default c'è una scelta selezionata, in realtà anche categoria e fascia sono superflui perchè è sempre selezionato qualcosa

                    AdapterConsegneItem ad = new AdapterConsegneItem();
                    ad.setDescrizione(descrizione); newConsegna.setDescrizione(descrizione);
                    ad.setCategoria(categoria); newConsegna.setCategoria(Categoria.getID(categoria));
                    String[] f=fascia.split(":"); //prende solo nome
                    ad.setFascia(f[0]); newConsegna.setFascia(f[0]);
                    RadioButton yes=findViewById(R.id.radioYes);

                    if(yes.isChecked()) {
                        magazzino = "true"; newConsegna.settingMagazzino(true);
                    }else {
                        magazzino = "false"; newConsegna.settingMagazzino(false);
                    }

                    ad.setMagazzino(magazzino); //contiene true/false
                    ad.setConsegna(newConsegna.getCodice());
                    newConsegna.impostaRitirata(false);

                    aggiungiConsegna(newConsegna,ad);

                }else{
                    Toast.makeText(getApplicationContext(), "Inserire i campi mancanti", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private synchronized void aggiungiConsegna(ConsegneItem r,AdapterConsegneItem ad){
        int i;
        if(indici == null || indici.size()==0) //determina id
            i = 1;
        else
            i = indici.last()+1;

        FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + i).setValue(r);
        Log.d(TAG,"Inserisco: "+r.getCodice());

        Toast.makeText(getApplicationContext(), "Nuova consegna inserita", Toast.LENGTH_LONG).show();
        Intent intent = new Intent();
        intent.putExtra(ConsegneItem.CONSEGNA, String.valueOf(i));
        intent.putExtra(ConsegneItem.CODICE,r.getCodice());
        intent.putExtra(ConsegneItem.IDCLIENTE,r.getCliente());
        intent.putExtra(ConsegneItem.CLIENTE,cliente);
        intent.putExtra(ConsegneItem.DATA,r.getData());
        intent.putExtra(ConsegneItem.FASCIA,ad.getFascia());
        intent.putExtra(ConsegneItem.CATEGORIA, ad.getCategoria());
        intent.putExtra(ConsegneItem.DESCRIZIONE, ad.getDescrizione());
        intent.putExtra(ConsegneItem.MAGAZZINO, ad.getMagazzino()); //contiene true/false
        intent.putExtra(ConsegneItem.RITIRATA,r.isRitirata()+""); //contiene true/false
        setResult(RESULT_OK, intent);
        a=3;
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("AAA",a+"");
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        setResult(RESULT_CANCELED);
        finish();
        return true;
    }
    @Override public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    public void aggiornaIDconsegne(){
        indici = new TreeSet<>();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Query lastItem = database.getReference().child(ConsegneItem.CONSEGNA).orderByKey().limitToLast(1);
        lastItem.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                indici.add(Integer.parseInt(dataSnapshot.getKey()));
            }
            @Override public void onChildChanged(DataSnapshot dataSnapshot, String s) { }
            @Override public void onChildRemoved(DataSnapshot dataSnapshot) { }
            @Override public void onChildMoved(DataSnapshot dataSnapshot, String s) { }
            @Override public void onCancelled(DatabaseError databaseError) { }
        });
    }

}