package ereale.uninsubria.disp_mobili.appriuso;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class RecyclerAdapterNewRitiro extends RecyclerView.Adapter<RecyclerAdapterNewRitiro.RitiroItemHolder>{
    private static ArrayList<AdapterRitiroItem> mRitiroItems;
    public RecyclerAdapterNewRitiro(ArrayList<AdapterRitiroItem> items){mRitiroItems = items;}
    // The adapter will work out how many items to display.
    @Override
    public int getItemCount() {return mRitiroItems.size();}
    // When there are no ViewHolders available. Then RecylerView asks to onCreateViewHolder() to make a new one. The item layout is used to create a view for the ViewHolder.
    @Override
    public RecyclerAdapterNewRitiro.RitiroItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_newritiro, parent, false);
        return new RitiroItemHolder(inflatedView);
    }
    // Passing in a copy of your ViewHolder and the position where the item will show in your RecyclerView.
    @Override
    public void onBindViewHolder(RecyclerAdapterNewRitiro.RitiroItemHolder holder, int position) {
        holder.bindTodoItem(mRitiroItems.get(position));
    }
    public static class RitiroItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {// 1
        // 2
        private TextView mItemDescrizione,mItemOfferta, mItemConsegna;
        private ImageView mItemImg;

        public RitiroItemHolder(View v) {//3
            super(v);
            mItemOfferta = v.findViewById(R.id.txtOfferta);
            mItemDescrizione = v.findViewById(R.id.editTxtDescrizione);
            mItemConsegna = v.findViewById(R.id.spinnerConsegna);
            mItemImg = v.findViewById(R.id.imageViewProdotto);
            v.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {// 4
        }
        public void bindTodoItem(AdapterRitiroItem item) {// 5
            mItemOfferta.setText(item.getOfferta());
            mItemDescrizione.setText(item.getDescrizione());
            mItemConsegna.setText(item.getConsegna());
            Categoria.immagineCategoria(mItemImg, item.getCategoria());
        }
    }
}