package ereale.uninsubria.disp_mobili.appriuso;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class ConritMesiFragment extends Fragment {
    private TextView TxtMediaConsegneAnni, TxtMediaRitiriAnni;
    private Spinner spinnerAnni;
    private LineChart lineChart;
    private List<Integer> anni;
    private ArrayAdapter<Integer> spinnerAdapter;
    private String[] mesi = {"Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"};
    private TreeMap<Integer,int[]> ritiriAnniMesi, consegneAnniMesi;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            view = inflater.inflate(R.layout.fragment_conrit_mesi, container, false);
        }else{
            view = inflater.inflate(R.layout.fragment_conrit_mesi1, container, false);
        }
        lineChart = view.findViewById(R.id.chart);
        TxtMediaConsegneAnni = view.findViewById(R.id.txtMediaCons);
        TxtMediaRitiriAnni = view.findViewById(R.id.txtMediaRit);
        spinnerAnni = view.findViewById(R.id.spinnerAnno);
        spinnerAdapter= new ArrayAdapter<>(view.getContext(), R.layout.support_simple_spinner_dropdown_item);
        riempiSpinnerAnni();
        spinnerAnni.setAdapter(spinnerAdapter);

        spinnerAnni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Elemento selezionato: parent.getItemAtPosition(position).toString()
                creaGrafico(Integer.parseInt(parent.getItemAtPosition(position).toString()));
            }
            @Override public void onNothingSelected (AdapterView < ? > arg0){}
        });
        return view;
    }
    private void riempiSpinnerAnni(){
        anni = new ArrayList<>();
        ritiriAnniMesi = new TreeMap<>();
        consegneAnniMesi = new TreeMap<>();
        DatabaseReference myRefRit = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO);
        myRefRit.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null) {
                    for(DataSnapshot d : dataSnapshot.getChildren()) {
                        StringTokenizer data = new StringTokenizer(d.child(RitiroItem.DATA).getValue().toString(), "-");
                        data.nextToken();
                        int mese = Integer.parseInt(data.nextToken());
                        int anno = Integer.parseInt(data.nextToken());

                        riempiAnni(anno);
                        int[] c = ritiriAnniMesi.get(anno);
                        ++c[mese-1];
                        ritiriAnniMesi.put(anno, c);
                    }
                    DatabaseReference myRefCon = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
                    myRefCon.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot!=null) {
                                for (DataSnapshot d : dataSnapshot.getChildren()) {
                                    StringTokenizer data = new StringTokenizer(d.child(ConsegneItem.DATA).getValue().toString(), "-");
                                    data.nextToken();
                                    int mese = Integer.parseInt(data.nextToken());
                                    int anno = Integer.parseInt(data.nextToken());

                                    riempiAnni(anno);
                                    int[] c = consegneAnniMesi.get(anno);
                                    ++c[mese-1];

                                    consegneAnniMesi.put(anno, c);
                                }
                                Collections.sort(anni);
                                spinnerAdapter.addAll(anni);
                            }
                        }
                        @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                    });
                }
            }
            @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }
    private void creaGrafico(int anno){
        ArrayList<ILineDataSet> dataSet;
        ArrayList<Entry> entryListRitiri = new ArrayList<>();
        ArrayList<Entry> entryListConsegne = new ArrayList<>();
        Collections.sort(anni);
        double mediaRit=0.0, mediaCon=0.0;
        for(int i=0;i<mesi.length;i++){
            mediaRit+=ritiriAnniMesi.get(anno)[i];
            entryListRitiri.add(new Entry(ritiriAnniMesi.get(anno)[i],i));
            mediaCon+=consegneAnniMesi.get(anno)[i];
            entryListConsegne.add(new Entry(consegneAnniMesi.get(anno)[i],i));
        }
        mediaCon = mediaCon/12;
        mediaRit = mediaRit/12;
        TxtMediaRitiriAnni.setText(new DecimalFormat("#.###").format(mediaRit));
        TxtMediaConsegneAnni.setText(new DecimalFormat("#.###").format(mediaCon));

        LineDataSet dataSetRitiri = new LineDataSet(entryListRitiri,"Ritiri");
        dataSetRitiri.setColor(view.getContext().getResources().getColor(R.color.colorRitiri));
        dataSetRitiri.setLineWidth(3f);
        dataSetRitiri.setDrawCircleHole(false);
        dataSetRitiri.setCircleColor(view.getContext().getResources().getColor(R.color.colorRitiri));
        LineDataSet dataSetConsegne = new LineDataSet(entryListConsegne,"Consegne");
        dataSetConsegne.setColor(view.getContext().getResources().getColor(R.color.colorConsegne));
        dataSetConsegne.setLineWidth(3f);
        dataSetConsegne.setDrawCircleHole(false);
        dataSetConsegne.setCircleColor(view.getContext().getResources().getColor(R.color.colorConsegne));
        dataSet = new ArrayList<>();
        dataSet.add(dataSetConsegne);
        dataSet.add(dataSetRitiri);

        LineData lineData = new LineData(mesi,dataSet);
        lineData.setDrawValues(false);
        lineChart.setData(lineData);
        lineChart.getLegend().setEnabled(false);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.animateY(2000);
        lineChart.invalidate();
        lineChart.setDescription("");
        lineChart.getXAxis().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getXAxis().setAxisLineWidth(1.3f);
        lineChart.getXAxis().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisLeft().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisLeft().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisLeft().setAxisLineWidth(2f);
        lineChart.getAxisLeft().setAxisMinValue(0f);
        lineChart.getAxisRight().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisRight().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisRight().setAxisLineWidth(2f);
        lineChart.getAxisRight().setAxisMinValue(0f);
    }
    private void riempiAnni(int anno) {
        Collections.sort(anni);
        if (anni.size() == 0){
            for(int i = anno; i<= Calendar.getInstance().get(Calendar.YEAR); i++) {
                anni.add(i);
                ritiriAnniMesi.put(i, new int[12]);
                consegneAnniMesi.put(i, new int[12]);
                int[] vR = new int[12];
                int[] vC = new int[12];
                for(int c=0;c<12;c++){
                    vR[c]=0;
                    vC[c]=0;
                }
                ritiriAnniMesi.put(i,vR);
                consegneAnniMesi.put(i,vC);
            }
        }else{
            int first = anni.get(0);
            if (anno < first) {
                for (int i =anno; i < first; i++) {
                    anni.add(i);
                    ritiriAnniMesi.put(i, new int[12]);
                    consegneAnniMesi.put(i, new int[12]);
                    int[] vR = new int[12];
                    int[] vC = new int[12];
                    for(int c=0;c<12;c++){
                        vR[c]=0;
                        vC[c]=0;
                    }
                    ritiriAnniMesi.put(i,vR);
                    consegneAnniMesi.put(i,vC);
                }
            }
        }
    }
}
