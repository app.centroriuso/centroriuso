package ereale.uninsubria.disp_mobili.appriuso;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import java.util.TreeSet;

public class NewCliente extends AppCompatActivity {
    private FirebaseDatabase database;
    private String cid, nome, cognome, tessera, activityChiamante;
    private static TreeSet<String> carteIdentita;
    private static TreeSet<String> tessere;
    private static TreeSet<Integer> indici;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.new_cliente);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.new_cliente1);
        }

        EditText n = findViewById(R.id.editTxtCId);
        n.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);

        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);

        Intent intent = getIntent();
        activityChiamante = intent.getStringExtra("chiamante");
        //if(activityChiamante.equals("ClienteActivity") || activityChiamante.equals("NewRitiro")) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       /* }else{
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }*/
        database = FirebaseDatabase.getInstance();
        riempiAlberi();
        Button bttAggiungi = findViewById(R.id.buttonAggiungiCliente);
        bttAggiungi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText app;
                app=findViewById(R.id.editTxtNome); nome = app.getText().toString();
                app=findViewById(R.id.editTxtCognome); cognome = app.getText().toString();
                app=findViewById(R.id.editTxtTessera); tessera = app.getText().toString();
                app=findViewById(R.id.editTxtCId); cid = app.getText().toString();

                if(!nome.equals("") && !cognome.equals("")&& !tessera.equals("") && !cid.equals("")) {
                    if(!controlloCarta(cid) && !controlloTessera(tessera)) {
                        insertCliente(new ClienteItem(cid.toUpperCase(), nome, cognome, tessera));
                        if(activityChiamante.equals("NewRitiriActivity") || activityChiamante.equals("NewConsegneActivity")){
                            Intent result = new Intent();
                            result.putExtra(ClienteItem.NOME, nome);
                            result.putExtra(ClienteItem.COGNOME, cognome);
                            result.putExtra("indice", (indici.last()+1));
                            setResult(RESULT_OK, result);
                        }
                        finish();
                    }else if(controlloCarta(cid)){
                        Toast.makeText(getApplicationContext(), "Carta d'identità già presente", Toast.LENGTH_LONG).show();
                    }else if(controlloTessera(tessera))
                        Toast.makeText(getApplicationContext(), "Tessera già presente", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Inserire i campi mancanti", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private synchronized void insertCliente(ClienteItem item){
        DatabaseReference myRef = database.getReference(ClienteItem.CLIENTE+"/"+(indici.last()+1));
        myRef.setValue(item);
        Toast.makeText(getApplicationContext(), "nuovo cliente inserito", Toast.LENGTH_LONG).show();
    }
    public static boolean controlloCarta(String cid){
        if(carteIdentita==null)
            riempiAlberi();
        return carteIdentita.contains(cid.toUpperCase());
    }
    public static boolean controlloTessera(String tessera){
        if(tessere==null)
            riempiAlberi();
        return tessere.contains(tessera.toUpperCase());
    }
    public static void riempiAlberi(){
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        carteIdentita = new TreeSet<>();
        tessere = new TreeSet<>();
        indici = new TreeSet<>();
        Query leggiDb = db.getReference().child(ClienteItem.CLIENTE);
        leggiDb.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                carteIdentita.add(dataSnapshot.child(ClienteItem.CARTA_ID).getValue().toString());
                tessere.add(dataSnapshot.child(ClienteItem.TESSERA).getValue().toString());
                indici.add(Integer.parseInt(dataSnapshot.getKey()));
            }
            @Override public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
            @Override public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EditText app;
        app=findViewById(R.id.editTxtNome); nome = app.getText().toString();
        app=findViewById(R.id.editTxtCognome); cognome = app.getText().toString();
        app=findViewById(R.id.editTxtTessera); tessera = app.getText().toString();
        app=findViewById(R.id.editTxtCId); cid = app.getText().toString();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EditText app;
        app=findViewById(R.id.editTxtNome); app.setText(nome);
        app=findViewById(R.id.editTxtCognome); app.setText(cognome);
        app=findViewById(R.id.editTxtTessera); app.setText(tessera);
        app=findViewById(R.id.editTxtCId); app.setText(cid);
    }
}