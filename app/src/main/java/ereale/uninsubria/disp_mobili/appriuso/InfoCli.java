package ereale.uninsubria.disp_mobili.appriuso;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

public class InfoCli extends AppCompatActivity {
    private String nome, cognome, cartaId, tessera;
    private EditText txtCid, txtNome, txtCognome, txtTessera;
    private Intent resultIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.info_cliente);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.info_cliente1);
        }

        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        nome = intent.getStringExtra(ClienteItem.NOME);
        txtNome = findViewById(R.id.editTxtNome);
        txtNome.setText(nome);
        cognome = intent.getStringExtra(ClienteItem.COGNOME);
        txtCognome = findViewById(R.id.editTxtCognome);
        txtCognome.setText(cognome);
        tessera = intent.getStringExtra(ClienteItem.TESSERA);
        txtTessera = findViewById(R.id.editTxtTessera);
        txtTessera.setText(tessera);
        cartaId = intent.getStringExtra(ClienteItem.CARTA_ID);
        txtCid = findViewById(R.id.editTxtCId);
        txtCid.setText(cartaId);

        resultIntent = new Intent();

        Button salvaModifiche = findViewById(R.id.buttonModifiche);
        salvaModifiche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resultIntent.putExtra(ClienteItem.NOME, txtNome.getText().toString());
                resultIntent.putExtra(ClienteItem.COGNOME, txtCognome.getText().toString());
                resultIntent.putExtra(ClienteItem.TESSERA, txtTessera.getText().toString());
                resultIntent.putExtra(ClienteItem.CARTA_ID, txtCid.getText().toString());
                if(controllaValori()) {
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
            }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        backSalvareModifiche();
        return true;
    }

    @Override
    public void onBackPressed() {
        backSalvareModifiche();
    }
    private void backSalvareModifiche(){
        resultIntent.putExtra(ClienteItem.NOME, txtNome.getText().toString());
        resultIntent.putExtra(ClienteItem.COGNOME, txtCognome.getText().toString());
        resultIntent.putExtra(ClienteItem.TESSERA, txtTessera.getText().toString());
        resultIntent.putExtra(ClienteItem.CARTA_ID, txtCid.getText().toString());
        if(!nome.equals(txtNome.getText().toString()) || !cognome.equals(txtCognome.getText().toString()) || !tessera.equals(txtTessera.getText().toString()) || !cartaId.equals(txtCid.getText().toString())) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Vuoi salvare le modifiche?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(controllaValori()) {
                        setResult(RESULT_OK, resultIntent);
                        finish();
                    }
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setResult(RESULT_CANCELED, resultIntent);
                    finish();
                }
            });
            alert.create().show();
        }else{
            setResult(RESULT_CANCELED, resultIntent);
            finish();
        }
    }
    private boolean controllaValori(){
        if("".equals(txtNome.getText().toString()) || "".equals(txtCognome.getText().toString()) || "".equals(txtTessera.getText().toString()) || "".equals(txtCid.getText().toString())) {
            Toast.makeText(getApplicationContext(), "riempire tutti i campi", Toast.LENGTH_LONG).show();
            return false;
        }else{
            if(!cartaId.equals(txtCid.getText().toString()) && NewCliente.controlloCarta(txtCid.getText().toString())){
                Toast.makeText(getApplicationContext(), "Carta d'identità già presente", Toast.LENGTH_LONG).show();
                return false;
            }else if(!tessera.equals(txtTessera.getText().toString()) && NewCliente.controlloTessera(txtTessera.getText().toString())){
                Toast.makeText(getApplicationContext(), "Tessera già presente", Toast.LENGTH_LONG).show();
                return false;
            }else{
                return true;
            }
        }
    }
}
