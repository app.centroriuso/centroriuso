package ereale.uninsubria.disp_mobili.appriuso;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class GuadagniMesiFragment extends Fragment {
    private TextView TxtMediaRitiriAnni, TxtTotaleGuadagni;
    private Spinner spinnerAnni;
    private LineChart lineChart;
    private List<Integer> anni;
    private ArrayAdapter<Integer> spinnerAdapter;
    private String[] mesi = {"Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"};
    private TreeMap<Integer,float[]> ritiriAnniMesi;
    private View view;
    private int min;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            view = inflater.inflate(R.layout.fragment_guadagni_mesi, container, false);
        }else{
            view = inflater.inflate(R.layout.fragment_guadagni_mesi1, container, false);
        }
        lineChart = view.findViewById(R.id.chart);
        TxtMediaRitiriAnni = view.findViewById(R.id.txtMediaRit); //media dei guadagni in tutto nell'anno
        TxtTotaleGuadagni = view.findViewById(R.id.txtTot);
        spinnerAnni = view.findViewById(R.id.spinnerAnno);
        spinnerAdapter= new ArrayAdapter<>(view.getContext(), R.layout.support_simple_spinner_dropdown_item);
        riempiSpinnerAnni();
        spinnerAnni.setAdapter(spinnerAdapter);

        spinnerAnni.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Elemento selezionato: parent.getItemAtPosition(position).toString()
                creaGrafico(Integer.parseInt(parent.getItemAtPosition(position).toString()));
            }
            @Override public void onNothingSelected (AdapterView < ? > arg0){}
        });
        return view;
    }
    private void riempiSpinnerAnni(){
        anni = new ArrayList<>();
        ritiriAnniMesi = new TreeMap<>();
        DatabaseReference myRefRit = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO);
        myRefRit.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null) {
                    min=Calendar.getInstance().get(Calendar.YEAR);
                    DatabaseReference myRefCons = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
                    myRefCons.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                            for (DataSnapshot d : dataSnapshot1.getChildren()) {

                                StringTokenizer data = new StringTokenizer(d.child(ConsegneItem.DATA).getValue().toString(), "-");
                                data.nextToken();
                                data.nextToken();
                                int anno = Integer.parseInt(data.nextToken());
                                if (anno < min) min = anno;
                            }
                            riempiAnni(min);
                            spinnerAdapter.addAll(anni);
                            for(DataSnapshot d : dataSnapshot.getChildren()) {
                                StringTokenizer data = new StringTokenizer(d.child(RitiroItem.DATA).getValue().toString(), "-");
                                data.nextToken();
                                int mese = Integer.parseInt(data.nextToken());
                                int anno = Integer.parseInt(data.nextToken());

                                float[] c = ritiriAnniMesi.get(anno);
                                double offerta = Double.parseDouble(d.child(RitiroItem.OFFERTA).getValue().toString());
                                c[mese-1]+=offerta; //-1 perchè parte da 0 la posizione dell'array
                                ritiriAnniMesi.put(anno, c);
                            }
                        }
                        @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                    });
                }
            }
            @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }
    private void creaGrafico(int anno){
        ArrayList<ILineDataSet> dataSet;
        ArrayList<Entry> entryListGuadagni = new ArrayList<>();
        Collections.sort(anni);
        double mediaRit=0.0;
        for(int i=0;i<mesi.length;i++){
            mediaRit+=ritiriAnniMesi.get(anno)[i]; //somma di tutte le offerte di quell'anno nei vari mesi
            entryListGuadagni.add(new Entry(ritiriAnniMesi.get(anno)[i],i)); //aggiungo nella posizione del mese nell'array list il numero di offerte in quel mese
        }
        TxtTotaleGuadagni.setText(new DecimalFormat("#.#").format(mediaRit)+"€");
        mediaRit = mediaRit/12;
        TxtMediaRitiriAnni.setText(new DecimalFormat("#.###").format(mediaRit)+"€");

        LineDataSet dataSetRitiri = new LineDataSet(entryListGuadagni,"Guadagni");
        dataSetRitiri.setColor(view.getContext().getResources().getColor(R.color.colorGuadagno));
        dataSetRitiri.setLineWidth(3f);
        dataSetRitiri.setDrawCircleHole(false);
        dataSetRitiri.setCircleColor(view.getContext().getResources().getColor(R.color.colorGuadagno));
        dataSet = new ArrayList<>();
        dataSet.add(dataSetRitiri);

        LineData lineData = new LineData(mesi,dataSet);
        lineChart.setData(lineData);
        lineChart.getLegend().setEnabled(false);
        lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        lineChart.animateY(2000);
        lineChart.invalidate();
        lineChart.setDescription("");
        lineChart.getXAxis().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getXAxis().setAxisLineWidth(1.3f);
        lineChart.getXAxis().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisLeft().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisLeft().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisLeft().setAxisLineWidth(2f);
        lineChart.getAxisLeft().setAxisMinValue(0f);
        lineChart.getAxisRight().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisRight().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
        lineChart.getAxisRight().setAxisLineWidth(2f);
        lineChart.getAxisRight().setAxisMinValue(0f);
    }
    private void riempiAnni(int anno) {
        for(int i=anno;i<=Calendar.getInstance().get(Calendar.YEAR);i++) {
            anni.add(i);
            ritiriAnniMesi.put(i, new float[12]);
            float[] vR = new float[12];
            for(int c=0;c<12;c++){ //azzero l'array una posizione per ogni mese
                vR[c]=0f;
            }
            ritiriAnniMesi.put(i,vR);
        }
    }
}
