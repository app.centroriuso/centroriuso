package ereale.uninsubria.disp_mobili.appriuso;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class ConritAnniFragment extends Fragment {
    private TextView TxtMediaConsegneAnni, TxtMediaRitiriAnni;
    private LineChart lineChart;
    private TreeMap<Integer, Integer> ritiriAnni,consegneAnni;
    private List<Integer> anni;
    private double mediaRit=0.0, mediaCon=0.0;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            view = inflater.inflate(R.layout.fragment_conrit_anni, container, false);
        }else{
            view = inflater.inflate(R.layout.fragment_conrit_anni1, container, false);
        }
        lineChart = view.findViewById(R.id.chart);

        TxtMediaConsegneAnni=view.findViewById(R.id.txtMediaCons);
        TxtMediaRitiriAnni=view.findViewById(R.id.txtMediaRit);

        ritiriAnni = new TreeMap<>();
        consegneAnni = new TreeMap<>();
        anni = new ArrayList<>();
        final DatabaseReference myRefRit = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO);
        final DatabaseReference myRefCon = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        myRefRit.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null) {
                    for(DataSnapshot d : dataSnapshot.getChildren()) {
                        StringTokenizer data = new StringTokenizer(d.child(RitiroItem.DATA).getValue().toString(), "-");
                        data.nextToken(); data.nextToken();
                        int anno =Integer.parseInt(data.nextToken());
                        riempiAnni(anno);
                        int c = ritiriAnni.get(anno);
                        ritiriAnni.put(anno, ++c);
                    }
                    myRefCon.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot!=null){
                                for(DataSnapshot d : dataSnapshot.getChildren()) {
                                    StringTokenizer data = new StringTokenizer(d.child(ConsegneItem.DATA).getValue().toString(), "-");
                                    data.nextToken(); data.nextToken();
                                    int anno =Integer.parseInt(data.nextToken()) ;
                                    riempiAnni(anno);
                                    int c = consegneAnni.get(anno);
                                    consegneAnni.put(anno, ++c);
                                }
                                Collections.sort(anni);
                                ArrayList<ILineDataSet> dataSet;
                                ArrayList<Entry> entryListRitiri = new ArrayList<>();
                                ArrayList<Entry> entryListConsegne = new ArrayList<>();
                                for(int i=0;i<anni.size();i++){
                                    mediaRit+=ritiriAnni.get(anni.get(i));
                                    entryListRitiri.add(new Entry(ritiriAnni.get(anni.get(i)),i));
                                    mediaCon+=consegneAnni.get(anni.get(i));
                                    entryListConsegne.add(new Entry(consegneAnni.get(anni.get(i)),i));
                                }
                                mediaRit=mediaRit/anni.size();
                                TxtMediaRitiriAnni.setText(new DecimalFormat("#.###").format(mediaRit));
                                mediaCon=mediaCon/anni.size();
                                TxtMediaConsegneAnni.setText(new DecimalFormat("#.###").format(mediaCon));
                                LineDataSet dataSetRitiri = new LineDataSet(entryListRitiri,"Ritiri");
                                dataSetRitiri.setColor(view.getContext().getResources().getColor(R.color.colorRitiri));
                                dataSetRitiri.setLineWidth(3f);
                                dataSetRitiri.setDrawCircleHole(false);
                                dataSetRitiri.setCircleColor(view.getContext().getResources().getColor(R.color.colorRitiri));
                                LineDataSet dataSetConsegne = new LineDataSet(entryListConsegne,"Consegne");
                                dataSetConsegne.setColor(view.getContext().getResources().getColor(R.color.colorConsegne));
                                dataSetConsegne.setLineWidth(3f);
                                dataSetConsegne.setDrawCircleHole(false);
                                dataSetConsegne.setCircleColor(view.getContext().getResources().getColor(R.color.colorConsegne));
                                dataSet = new ArrayList<>();
                                dataSet.add(dataSetConsegne);
                                dataSet.add(dataSetRitiri);
                                ArrayList<String> anniS=new ArrayList<>();
                                for(int a : anni)
                                    anniS.add(String.valueOf(a));
                                LineData lineData = new LineData(anniS,dataSet);
                                lineData.setDrawValues(false);
                                lineChart.setData(lineData);
                                lineChart.getLegend().setEnabled(false);
                                lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                                lineChart.animateY(2000);
                                lineChart.invalidate();
                                lineChart.setDescription("");
                                lineChart.getXAxis().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                                lineChart.getXAxis().setAxisLineWidth(1.3f);
                                lineChart.getXAxis().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                                lineChart.getAxisLeft().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                                lineChart.getAxisLeft().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                                lineChart.getAxisLeft().setAxisLineWidth(2f);
                                lineChart.getAxisLeft().setAxisMinValue(0f);
                                lineChart.getAxisRight().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                                lineChart.getAxisRight().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                                lineChart.getAxisRight().setAxisLineWidth(2f);
                                lineChart.getAxisRight().setAxisMinValue(0f);
                            }
                        }
                        @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                    });
                }
            }
            @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        return view;
    }
    private void riempiAnni(int anno) {
        Collections.sort(anni);
        if (anni.size() == 0){
            for(int i=anno;i<=Calendar.getInstance().get(Calendar.YEAR);i++) {
                anni.add(i);
                ritiriAnni.put(i, 0);
                consegneAnni.put(i, 0);
            }
        }else{
            int first = anni.get(0);
            if (anno < first) {
                for (int i = anno ; i < first; i++) {
                    anni.add(i);
                    ritiriAnni.put(i, 0);
                    consegneAnni.put(i, 0);
                }
            }
        }
    }
}
