package ereale.uninsubria.disp_mobili.appriuso;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity implements View.OnClickListener  {
    private static String TAG = "MainActivity";
    private Intent intent;
    private FirebaseUser mFirebaseUser;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.activity_main);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_main1);
        }
        Toolbar myToolbar = findViewById(R.id.tool_bar);
        myToolbar.setElevation(0);
        setSupportActionBar(myToolbar);

        FirebaseAuth mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();


        //questo è il punto
        if (mFirebaseUser == null) {
            Intent intent = new Intent(this, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }
    @Override
    public void onClick(View view) {
        if(view == findViewById(R.id.buttonCliente) || view==findViewById(R.id.imageButtonCliente)){
            intent = new Intent(this, ClienteActivity.class);
            startActivity(intent);
        }else if(view == findViewById(R.id.buttonConsegne) || view==findViewById(R.id.imageButtonConsegna)){
            intent = new Intent(this, ConsegneActivity.class);
            startActivity(intent);
        }else if(view == findViewById(R.id.buttonRitiri) || view==findViewById(R.id.imageButtonRitiro)){
            //intent = new Intent(this, NewRitiriActivity.class);
            intent = new Intent(this, RitiroActivity.class);
            startActivity(intent);
        }else if(view == findViewById(R.id.buttonStatistiche) || view==findViewById(R.id.imageButtonStatistiche)){
            intent = new Intent(this, Statistiche.class);
            startActivity(intent);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu miomenu) {
        getMenuInflater().inflate(R.menu.menu_logout, miomenu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.action_logout){
            AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
            alert.setTitle("Logout");
            alert.setMessage("Sei sicuro di voler effettuare il logout?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FirebaseAuth.getInstance().signOut();
                    mFirebaseUser = null;
                    intent = new Intent(MainActivity.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });
            alert.create().show();
        }
        return true;
    }
}