package ereale.uninsubria.disp_mobili.appriuso;

public class AdapterRitiroItem { //classe per gli item della recycler della newRitiriActivity
    private String consegna; //codice consegna
    private String offerta;
    private String descrizione; //della consegna
    private String categoria; //della consegna
    private String idImg; //della categoria della consegna
    private int ritiro; //codice ritiro
    private int cliente; //codice cliente
    String nome; //nome e cognome del cliente

    public AdapterRitiroItem(String consegna, String offerta, String descrizione, String categoria, String imm) {
        this.consegna = consegna;
        this.offerta = offerta;
        this.descrizione = descrizione;
        this.categoria = categoria;
        this.idImg = imm;
        this.cliente=0;
    }
    public AdapterRitiroItem() {}
    public void setRitiro(int r){this.ritiro = r;}
    public int getRitiro(){return this.ritiro;}
    public void setCliente(int cli){this.cliente=cli;}
    public int getCliente(){return cliente;}

    public String getConsegna() {
        return consegna;
    }

    public void setConsegna(String consegna) {
        this.consegna = consegna;
    }

    public String getOfferta() {
        return offerta;
    }

    public void setOfferta(String offerta) {
        this.offerta = offerta;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }
    public String getIDImg(){
        return this.idImg;
    }

    public void decidiNomeCliente(String n){nome = n;}
    public String nomeCliente() {return nome;}
}
