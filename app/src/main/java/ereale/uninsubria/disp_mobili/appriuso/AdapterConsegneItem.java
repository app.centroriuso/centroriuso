package ereale.uninsubria.disp_mobili.appriuso;

public class AdapterConsegneItem { //classe per gli item della recycler della newConsegneActivity
    private String descrizione;
    private String categoria;
    private String idImg; //img categoria
    private String fascia;
    private String magazzino;
    private String consegna; //codice consegna
    private String id; //id consegna


    public AdapterConsegneItem(){}
    public AdapterConsegneItem(String descrizione, String categoria, String idImg,String fascia, String magazzino){
        this.categoria=categoria;
        this.descrizione=descrizione;
        this.fascia=fascia;
        this.idImg=idImg;
        this.magazzino=magazzino;
    }


    public String getId(){return id;}
    public void setId(String id){this.id=id;}

    public String getConsegna() {
        return consegna;
    }
    public void setConsegna(String consegna) {
        this.consegna = consegna;
    }

    public String getDescrizione() {
        return descrizione;
    }
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getMagazzino() {
        return magazzino;
    }
    public void setMagazzino(String magazzino) {
        this.magazzino = magazzino;
    }

    public String getCategoria() {
        return categoria;
    }
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getIdImg() {
        return idImg;
    }

    public String getFascia() {
        return fascia;
    }
    public void setFascia(String fascia) {
        this.fascia = fascia;
    }
}
