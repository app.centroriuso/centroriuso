package ereale.uninsubria.disp_mobili.appriuso;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.TreeMap;

public class InfoConsegne extends AppCompatActivity {
    private static final String TAG = "InfoConsegne";
    private RadioButton yes,no;
    private String cliente, descrizione, categoria, data, magazzino, fascia,ritirata;
    private EditText txtDescrizione;
    private Button salvaModifiche;
    private Intent resultIntent;
    Spinner categoriaSpinner, fasciaSpinner;
    RadioGroup radio_group;
    private ArrayAdapter<String> adapterCli;
    TreeMap<String, Integer> clienti = new TreeMap<>();
    private AutoCompleteTextView TxtCliente;
    private String codice;
    private DatePickerDialog.OnDateSetListener dataListener;
    int idCliente;

    /*calendario*/
    int year_x, month_x, day_x;
    static final int DIALOG_ID=0;
    //private TextView data;
    private TextView txtData;
    private Button btn;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.info_consegne);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.info_consegne1);
        }

        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        codice=intent.getStringExtra(ConsegneItem.CODICE);

        cliente = intent.getStringExtra(ConsegneItem.CLIENTE);
        leggiClienti();
        TxtCliente = findViewById(R.id.editTxtCliente); //errore qui
        adapterCli = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        TxtCliente.setAdapter(adapterCli);
        TxtCliente.setText(cliente);

        descrizione = intent.getStringExtra(ConsegneItem.DESCRIZIONE);
        txtDescrizione = findViewById(R.id.editTxtDescrizione);
        txtDescrizione.setText(descrizione);

        data = intent.getStringExtra(ConsegneItem.DATA);
        txtData = findViewById(R.id.txtData);
        String[] d=data.split("-");
        /*calendario*/
        final Calendar cal = Calendar.getInstance();
        year_x = Integer.parseInt(d[2]);
        month_x = Integer.parseInt(d[1])-1;
        day_x = Integer.parseInt(d[0]);
        showDialogOnButtonClick();

        txtData.setText(data);

        categoria=intent.getStringExtra(ConsegneItem.CATEGORIA);
        categoriaSpinner=(Spinner) findViewById(R.id.spinnerCategoria);
        ArrayAdapter<String> spinnerAdapter= new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.elenco_categoria));
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item); //ma potrebbe essere superfluo?
        categoriaSpinner.setAdapter(spinnerAdapter);
        int idCategoria=Categoria.getID(categoria);
        categoriaSpinner.setSelection(idCategoria-1);

        fascia=intent.getStringExtra(ConsegneItem.FASCIA);
        fasciaSpinner=(Spinner)findViewById(R.id.spinnerFascia);
        ArrayAdapter<String> spinnerAdapterFascia= new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item);
        spinnerAdapterFascia.addAll(Fascia.creaSpinner());
        spinnerAdapterFascia.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item); //ma potrebbe essere superfluo?
        fasciaSpinner.setAdapter(spinnerAdapterFascia);

        fasciaSpinner.setSelection(spinnerAdapterFascia.getPosition(Fascia.getfasciaDB(fascia).getNomeCompleto()));

        magazzino = intent.getStringExtra(ConsegneItem.MAGAZZINO);
        Log.d(TAG,magazzino);
        radio_group=findViewById(R.id.radioGroupMagazzino);
        yes = findViewById(R.id.radioYes);
        no=findViewById(R.id.radioNo);
        if(magazzino.equals("true")) {
            yes.setChecked(true);
            no.setChecked(false);
        }else{
            yes.setChecked(false);
            no.setChecked(true);
        }

        ritirata=intent.getStringExtra(ConsegneItem.RITIRATA);

        resultIntent = new Intent();

        salvaModifiche = findViewById(R.id.buttonSalvaModifiche);
        salvaModifiche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clienti.containsKey(TxtCliente.getText().toString())) {
                    animaIntent();
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(InfoConsegne.this);
                    alert.setTitle("Nuovo cliente");
                    alert.setMessage("Il cliente " + TxtCliente.getText().toString() + " non è registrato. Vuoi aggiungerlo?");
                    alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent cli = new Intent(InfoConsegne.this, NewCliente.class);
                            cli.putExtra("chiamante", "NewConsegneActivity");
                            startActivityForResult(cli, 2);
                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alert.create().show();
                }
            }
        });
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        backSalvareModifiche();
        return true;
    }

    /*Per calendario*/
    public void showDialogOnButtonClick(){
        btn=(Button) findViewById(R.id.buttonCalendario);
        btn.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        showDialog(DIALOG_ID);
                    }
                }
        );
    }
    @Override
    protected Dialog onCreateDialog(int id){
        if(id == DIALOG_ID)
            return new DatePickerDialog(this, dpickerListener, year_x, month_x, day_x);
        return null;
    }
    private DatePickerDialog.OnDateSetListener dpickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            year_x = year;
            month_x = month+1;
            day_x = dayOfMonth;
            txtData.setText(day_x+"-"+month_x+"-"+year_x);
        }
    };

    public void animaIntent(){

        resultIntent.putExtra(ConsegneItem.CODICE,codice);
        resultIntent.putExtra(ConsegneItem.CLIENTE, TxtCliente.getText().toString());
        Log.d(TAG,TxtCliente.getText().toString());
        Log.d(TAG,idCliente+"");
        resultIntent.putExtra(ConsegneItem.CATEGORIA, categoriaSpinner.getSelectedItem().toString());
        resultIntent.putExtra(ConsegneItem.DATA, txtData.getText().toString());
        resultIntent.putExtra(ConsegneItem.DESCRIZIONE, txtDescrizione.getText().toString());
        String fas=fasciaSpinner.getSelectedItem().toString();
        resultIntent.putExtra(ConsegneItem.FASCIA, Fascia.getFascia(fas).getNome());
        RadioButton yes=findViewById(R.id.radioYes);
        String m;
        if(yes.isChecked()) {
            m = "true";
        }else {
            m = "false";
        }
        resultIntent.putExtra(ConsegneItem.MAGAZZINO, m); //passo true/false
        resultIntent.putExtra(ConsegneItem.RITIRATA,ritirata); //perchè posso modificarlo solo da ritiro
    }

    @Override
    public void onBackPressed() {
        backSalvareModifiche();
    }
    private void backSalvareModifiche(){

        String categoria = categoriaSpinner.getSelectedItem().toString();
        String f=Fascia.getFascia(fasciaSpinner.getSelectedItem().toString()).getNome();

        if(!descrizione.equals("")&& !categoria.equals("") && !fascia.equals("") && !TxtCliente.getText().toString().equals("")) { //non controllo magazzino e data perchè di default c'è una scelta selezionata, in realtà anche categoria e fascia sono superflui perchè è sempre selezionato qualcosa

            animaIntent();

            if (!cliente.equals(TxtCliente.getText().toString()) || !this.categoria.equals(categoria) || !this.fascia.equals(f) || !descrizione.equals(txtDescrizione.getText().toString()) || !data.equals(txtData.getText().toString()) || ((magazzino.equals("true") && no.isChecked())||(magazzino.equals("false") && yes.isChecked()))) {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Vuoi salvare le modifiche?");
                alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if(clienti.containsKey(TxtCliente.getText().toString())) {

                            setResult(RESULT_OK, resultIntent);
                            finish();
                        }else {
                            AlertDialog.Builder alert = new AlertDialog.Builder(InfoConsegne.this);
                            alert.setTitle("Nuovo cliente");
                            alert.setMessage("Il cliente " + TxtCliente.getText().toString() + " non è registrato. Vuoi aggiungerlo?");
                            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent cli = new Intent(InfoConsegne.this, NewCliente.class);
                                    cli.putExtra("chiamante", "NewConsegneActivity");
                                    startActivityForResult(cli, 2);
                                }
                            });
                            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alert.create().show();
                        }
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setResult(RESULT_CANCELED, resultIntent);
                        finish();
                    }
                });
                alert.create().show();
            } else {
                setResult(RESULT_CANCELED, resultIntent);
                finish();
            }
        }else{
            Toast.makeText(getApplicationContext(), "Specificare tutti i campi", Toast.LENGTH_LONG).show();
        }
    }

    private void leggiClienti(){
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
        Query query = myRef.orderByChild(ClienteItem.NOME);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String nc;
                    for(DataSnapshot d : dataSnapshot.getChildren()) {
                        nc="";
                        nc += d.child(ClienteItem.NOME).getValue().toString() + " " + d.child(ClienteItem.COGNOME).getValue().toString();
                        clienti.put(nc, Integer.parseInt(d.getKey()));
                    }
                    adapterCli.addAll(clienti.keySet());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

}