package ereale.uninsubria.disp_mobili.appriuso;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class NewRitiriActivity extends AppCompatActivity{
    private TreeMap<String, Integer> clienti;
    private RecyclerView mRecyclerView;
    private RecyclerAdapterNewRitiro mAdapter;
    private ArrayList<AdapterRitiroItem> ritiroItems;
    private ArrayAdapter<String> adapterCli;
    private AutoCompleteTextView TxtCliente;
    private boolean uscito, setdata;
    private String chiamante, consegnaChiamata;

    /*calendario*/
    int year_x, month_x, day_x;
    static final int DIALOG_ID=0;
    private TextView data;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ritiro);
        ritiroItems = new ArrayList<>();

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        uscito=false;
        setdata=false;

        /**PER CHIAMATA DA InfoConsNoMod*/
        Intent startIntent = getIntent();
        chiamante = startIntent.getStringExtra("chiamante");
        if(chiamante!=null && chiamante.equals("InfoConsNoMod")){
            consegnaChiamata = startIntent.getStringExtra(ConsegneItem.CODICE);
        }

        mRecyclerView = findViewById(R.id.recyclerView);
        mAdapter = new RecyclerAdapterNewRitiro(ritiroItems);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        setRecyclerViewItemTouchListener();

        leggiClienti();
        TxtCliente = findViewById(R.id.editTxtCliente);
        adapterCli = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        TxtCliente.setAdapter(adapterCli);

        Button registraCliente = findViewById(R.id.buttonAggiungiCliente);
        registraCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cli = new Intent(NewRitiriActivity.this, NewCliente.class);
                cli.putExtra("chiamante","NewRitiriActivity" );
                startActivityForResult(cli, 2);
            }
        });

        /*calendario*/
        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);
        showDialogOnButtonClick();
        data = findViewById(R.id.txtData);
        String oggi = day_x+"-"+(month_x+1)+"-"+year_x;
        data.setText(oggi);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
                Query query = myRef.orderByChild(ConsegneItem.RITIRATA).equalTo(false);
                query.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.getChildrenCount() > 0){
                            Intent mIntent = new Intent(NewRitiriActivity.this, NewRitiro.class);
                            if (TxtCliente.getText().toString().equals("")) {
                                Toast.makeText(getApplicationContext(), "Specificare il cliente", Toast.LENGTH_LONG).show();
                            } else if(clienti.containsKey(TxtCliente.getText().toString())){
                                mIntent.putExtra(RitiroItem.CLIENTE, String.valueOf(clienti.get(TxtCliente.getText().toString())));
                                mIntent.putExtra("NOME"+RitiroItem.CLIENTE, TxtCliente.getText().toString());
                                mIntent.putExtra(RitiroItem.DATA, data.getText().toString());
                                /**PER CHIAMATA DA InfoConsNoMod*/
                                if(chiamante!=null)
                                    mIntent.putExtra(RitiroItem.CONSEGNA,consegnaChiamata);

                                startActivityForResult(mIntent, 1);
                            }else{
                                AlertDialog.Builder alert = new AlertDialog.Builder(NewRitiriActivity.this);
                                alert.setTitle("Nuovo cliente");
                                alert.setMessage("Il cliente "+TxtCliente.getText().toString()+" non è registrato. Vuoi aggiungerlo?");
                                alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent cli = new Intent(NewRitiriActivity.this, NewCliente.class);
                                        cli.putExtra("chiamante","NewRitiriActivity" );
                                        startActivityForResult(cli, 2);
                                    }
                                });
                                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override public void onClick(DialogInterface dialog, int which) {}
                                });
                                alert.create().show();
                            }
                        }else
                            Toast.makeText(getBaseContext(), "Non ci sono consegne da ritirare", Toast.LENGTH_LONG).show();
                    }
                    @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                });

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode == RESULT_OK) {
            AdapterRitiroItem rit = new AdapterRitiroItem();
            rit.setConsegna(data.getStringExtra(RitiroItem.CONSEGNA));
            rit.setOfferta(data.getStringExtra(RitiroItem.OFFERTA));
            rit.setDescrizione(data.getStringExtra(ConsegneItem.DESCRIZIONE));
            rit.setCategoria(data.getStringExtra(ConsegneItem.CATEGORIA));
            rit.setRitiro(Integer.parseInt(data.getStringExtra(RitiroItem.RITIRO)));
            rit.setCliente(Integer.parseInt(data.getStringExtra(RitiroItem.CLIENTE)));
            ritiroItems.add(rit);
            mAdapter.notifyItemInserted(ritiroItems.size());
        }else if(requestCode==2 && resultCode == RESULT_OK){
            String nome = data.getStringExtra(ClienteItem.NOME);
            String cognome = data.getStringExtra(ClienteItem.COGNOME);
            int indice = data.getIntExtra("indice", 1);
            clienti.put(nome+" "+cognome, indice);
            adapterCli.add(nome+" "+cognome);
            TxtCliente.setText(nome+" "+cognome);
        }
    }

    private void leggiClienti(){
        clienti = new TreeMap<>();
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
        Query query = myRef.orderByChild(ClienteItem.NOME);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String nc;
                    for(DataSnapshot d : dataSnapshot.getChildren()) {
                        nc="";
                        nc += d.child(ClienteItem.NOME).getValue().toString() + " " + d.child(ClienteItem.COGNOME).getValue().toString();
                        clienti.put(nc, Integer.parseInt(d.getKey()));
                    }
                    adapterCli.addAll(clienti.keySet());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu miomenu) {
        getMenuInflater().inflate(R.menu.menu_carrello, miomenu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        final Intent resIntent=new Intent();
        resIntent.putExtra(ConsegneItem.RITIRATA,"false");
        if(id == R.id.action_buy){
            if(ritiroItems.size()>0 && TxtCliente.getText().toString() != null) {
                final int cliente = clienti.get(TxtCliente.getText().toString());
                for (AdapterRitiroItem itm : ritiroItems) {
                    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO + "/" + itm.getRitiro());
                    myRef.setValue(new RitiroItem(itm.getConsegna(), data.getText().toString(), cliente, Double.parseDouble(itm.getOfferta())));
                    if(chiamante!=null && consegnaChiamata!=null && chiamante.equals("InfoConsNoMod") && itm.getConsegna().equals(consegnaChiamata))
                        resIntent.putExtra(ConsegneItem.RITIRATA,"true");

                }
                DatabaseReference myRefRit = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO);
                Query queryR = myRefRit.orderByChild(RitiroItem.CLIENTE).equalTo(cliente);
                queryR.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        double sum = 0;
                        for (AdapterRitiroItem itm : ritiroItems)
                            sum += Double.parseDouble(itm.getOfferta());
                        int count=0;
                        int mContr;
                        if(!setdata)
                            mContr=month_x+1;
                        else
                            mContr=month_x;
                        for(DataSnapshot d : dataSnapshot.getChildren()){
                            StringTokenizer data = new StringTokenizer(d.child(RitiroItem.DATA).getValue().toString(), "-");
                            data.nextToken();
                            int m = Integer.parseInt(data.nextToken());
                            int a = Integer.parseInt(data.nextToken());
                            if (m == mContr && a == year_x)
                                count++;
                        }
                        AlertDialog.Builder alert = new AlertDialog.Builder(NewRitiriActivity.this);
                        alert.setTitle("Comprare?");
                        DateFormatSymbols dfs = new DateFormatSymbols();
                        String[] months = dfs.getMonths();
                        alert.setMessage("Prezzo totale: " + sum+"\n("+TxtCliente.getText().toString()+" ha effettuato "+(count-ritiroItems.size())+" ritiri in "+months[mContr-1]+")");
                        alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //for (AdapterRitiroItem itm : ritiroItems){
                                //  DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO + "/" + itm.getRitiro());
                                //myRef.setValue(new RitiroItem(itm.getConsegna(),data.getText().toString(),cliente,Double.parseDouble(itm.getOfferta())));
                                //}
                                ArrayList<String> consCliente=new ArrayList<>();
                                for(AdapterRitiroItem ari : ritiroItems) {
                                    if (ari.getCliente() == cliente)
                                        consCliente.add(ari.getConsegna());
                                }
                                if(consCliente.size()>0){
                                    AlertDialog.Builder alert = new AlertDialog.Builder(NewRitiriActivity.this);
                                    alert.setTitle("Sei sicuro di comprare?");
                                    String message=TxtCliente.getText().toString()+" ha effettuato la consegna: ";
                                    for(String c : consCliente)
                                        message+=c;
                                    alert.setMessage(message);
                                    alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            uscito=true;
                                            if(chiamante.equals("InfoConsNoMod"))
                                                setResult(RESULT_OK, resIntent);
                                            finish();
                                        }
                                    });
                                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        @Override public void onClick(DialogInterface dialog, int which) {}
                                    });
                                    alert.create().show();
                                }else{
                                    uscito=true;
                                    if(chiamante!=null && chiamante.equals("InfoConsNoMod"))
                                        setResult(RESULT_OK, resIntent);
                                    finish();
                                }
                            }
                        });
                        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override public void onClick(DialogInterface dialog, int which) {}
                        });
                        alert.create().show();
                    }
                    @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }else
                Toast.makeText(getApplicationContext(), "Nessun ordine specificato", Toast.LENGTH_LONG).show();
        }else if(id == android.R.id.home){
            esc();
        }
        return true;
    }
    @Override public void onBackPressed() {esc();}
    private void esc(){
        if(ritiroItems.size()>0) {
            AlertDialog.Builder alert = new AlertDialog.Builder(NewRitiriActivity.this);
            alert.setTitle("Annullare l'ordine?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    for (AdapterRitiroItem itm : ritiroItems) {
                        FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO + "/" + itm.getRitiro()).removeValue();
                        DatabaseReference reference=FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
                        Query query=reference.orderByChild(ConsegneItem.CODICE).equalTo(itm.getConsegna());
                        query.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                String codice=dataSnapshot.getKey();
                                DatabaseReference refCon = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + codice + "/" + ConsegneItem.RITIRATA);
                                refCon.setValue(false);
                            }

                            @Override
                            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                            @Override
                            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}

                            @Override
                            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}
                        });

                    }
                    uscito=true;
                    if(chiamante!=null && chiamante.equals("InfoConsNoMod"))
                        setResult(RESULT_CANCELED, new Intent());
                    finish();
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override public void onClick(DialogInterface dialog, int which) {}
            });
            alert.create().show();
        }else {
            uscito=true;
            finish();
        }
    }

    private void setRecyclerViewItemTouchListener() {
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
        {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                // 2 - You return false in onMove because you don’t want to perform any special behavior here.
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                // 3 - onSwiped is called when you swipe an item in the direction specified in the
                // ItemTouchHelper. Here, you request the viewHolder parameter passed for the
                // position of the item view, then you remove that item from your list of photos.
                // Finally, you inform the RecyclerView adapter that an item has been removed at a
                // specific position.
                int position = viewHolder.getAdapterPosition();
                AdapterRitiroItem item = ritiroItems.remove(position);
                // delete the item from the DB
                FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO + "/" + item.getRitiro()).removeValue();
                DatabaseReference reference=FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
                Query query=reference.orderByChild(ConsegneItem.CODICE).equalTo(item.getConsegna());
                query.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        String codice=dataSnapshot.getKey();
                        DatabaseReference refCon = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + codice + "/" + ConsegneItem.RITIRATA);
                        refCon.setValue(false);
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });

                mRecyclerView.getAdapter().notifyItemRemoved(position);
            }
        };
        // 4 - You initialize the ItemTouchHelper with the callback behavior you defined, and
        // then attach it to the RecyclerView.
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    /*Per calendario*/
    public void showDialogOnButtonClick(){
        Button btn=(Button) findViewById(R.id.buttonCalendario);
        btn.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        showDialog(DIALOG_ID);
                    }
                }
        );
    }
    @Override
    protected Dialog onCreateDialog(int id){
        if(id == DIALOG_ID)
            return new DatePickerDialog(this, dpickerListener, year_x, month_x, day_x);
        return null;
    }
    private DatePickerDialog.OnDateSetListener dpickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            setdata=true;
            year_x = year;
            month_x = month+1;
            day_x = dayOfMonth;
            data.setText(day_x+"-"+month_x+"-"+year_x);
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("salvato",!uscito);
        if(!uscito) {
            outState.putInt("num", ritiroItems.size());
            int i = 0;
            for (AdapterRitiroItem r : ritiroItems) {
                outState.putString(RitiroItem.CONSEGNA + "" + i, r.getConsegna());
                outState.putInt("ritiro" + i, r.getRitiro());
                outState.putString(ConsegneItem.DESCRIZIONE + "" + i, r.getDescrizione());
                outState.putString(RitiroItem.OFFERTA + "" + i, r.getOfferta());
                outState.putString(ConsegneItem.CATEGORIA + "" + i, r.getCategoria());
                outState.putString("img" + i, r.getIDImg());
                outState.putInt(RitiroItem.CLIENTE+""+i,r.getCliente());
                i++;
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState.getBoolean("salvato")){
            int num = savedInstanceState.getInt("num");
            for(int i=0;i<num;i++){
                AdapterRitiroItem ad = new AdapterRitiroItem();
                ad.setCategoria(savedInstanceState.getString(ConsegneItem.CATEGORIA+""+i));
                ad.setConsegna(savedInstanceState.getString(ConsegneItem.CONSEGNA+""+i));
                ad.setDescrizione(savedInstanceState.getString(ConsegneItem.DESCRIZIONE+""+i));
                ad.setOfferta(savedInstanceState.getString(RitiroItem.OFFERTA+""+i));
                ad.setRitiro(savedInstanceState.getInt("ritiro"+i));
                ad.setCliente(savedInstanceState.getInt(RitiroItem.CLIENTE+""+i));
                ritiroItems.add(ad);
            }
        }
    }
}
