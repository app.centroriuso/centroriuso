package ereale.uninsubria.disp_mobili.appriuso;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class ConsegneItem {

    //dichiarazione attributi
    private int codice; //codice Consegna
    private String data; //data di consegna utilizzabile per chiave primaria
    private int cliente; //codice cliente
    private String descrizione; //descrizione prodotto
    private boolean magazzino; //true se è nel magazzino, false se è online
    private int categoria; //codice della categoria appartenente
    private String fascia; //nome della fascia
    private boolean ritirata; //true se è stato ritirato, false altrimenti
    private String nome; //nome e cognome del cliente

    //costanti
    static final String CONSEGNA = "consegna";
    static final String CODICE = "codice"; //serve?
    static final String CLIENTE = "cliente";
    static final String DESCRIZIONE = "descrizione";
    static final String CATEGORIA ="categoria";
    static final String MAGAZZINO = "magazzino";
    static final String DATA = "data";
    static final String FASCIA="fascia";
    static final String RITIRATA="ritirata";
    static final String IDCLIENTE="idCliente";


    //getter e setter per ogni attributo
    public  String getCodice() {
        String[] s=data.split("-");
        String c = codice+"/"+s[1]+"/"+s[2];
        return c;
    }
    public void setCodice(String codice){String[] c=codice.split("/"); this.codice=Integer.parseInt(c[0]);}

    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
        DeterminaProgressivo(data);
    }

    public int getCliente() {
        return cliente;
    }
    public void setCliente(int cliente) {
        this.cliente = cliente;
    }

    public void decidiNomeCliente(String n){nome = n;}
    public String nomeCliente() {return nome;}

    public String getDescrizione() {
        return descrizione;
    }
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public boolean isMagazzino() {
        return magazzino;
    }

    public boolean isRitirata() { return ritirata; }
    public void impostaRitirata(boolean ritirata){this.ritirata=ritirata;}

    public int getCategoria(){ return categoria; }
    public void setCategoria(int categoria){ this.categoria = categoria; }

    public void setFascia(String fascia){ this.fascia=fascia;}
    public String getFascia(){ return this.fascia; }

    public String magazzinoString(){
        if(magazzino)
            return "Magazzino";
        else
            return "No magazzino";
    }
    public void settingMagazzino(boolean magazzino){this.magazzino=magazzino;}


    //costruttore
    public ConsegneItem(){}
    public ConsegneItem(int cliente, String descrizione, int categoria, String data, boolean magazzino, String fascia){ //o passo tutti gli attributi di activity_prodotto1_nomod?
        this.cliente=cliente;
        this.descrizione=descrizione;
        this.categoria=categoria;
        this.data=data;
        this.magazzino=magazzino;
        this.fascia=fascia;
        this.ritirata=false;
        DeterminaProgressivo(data);
    }

    private synchronized void DeterminaProgressivo(final String data){
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        Query query = myRef.orderByChild(ConsegneItem.DATA);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String nc;
                    int day=00,cod=0;
                    String[] j=data.split("-");
                    for (DataSnapshot d : dataSnapshot.getChildren()) {
                        nc=""+d.child(ConsegneItem.DATA).getValue();
                        String[] i=nc.split("-");
                        if(i[1].equals(j[1]) && i[2].equals(j[2])){
                            if(day<=Integer.parseInt(i[0])){
                                String c=d.child(ConsegneItem.CODICE).getValue().toString();
                                String[] k=c.split("/");
                                if(cod<=Integer.parseInt(k[0])){
                                    cod=Integer.parseInt(k[0]);
                                    day=Integer.parseInt(i[0]);
                                }
                            }
                        }
                    }
                    codice=cod+1;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    public String toString(){
        return "Codice: "+getCodice()+"\n"+"Data: "+getData()+"\n"+"Descrizione: "+getDescrizione()+"\n"+"Cliente: "+getCliente()+"\n"+"Categoria: "+getCategoria()+"\n"+"Fascia: "+getFascia()+"\n"+"Magazzino: "+isMagazzino()+"\n"+"Ritirato: "+isRitirata();
    }

}
