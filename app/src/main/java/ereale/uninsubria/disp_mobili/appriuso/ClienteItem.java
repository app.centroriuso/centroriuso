package ereale.uninsubria.disp_mobili.appriuso;

public class ClienteItem {
    protected String cartaId;
    protected String nome;
    protected String cognome;
    protected String tessera; //numero tessera centro

    static final String CLIENTE = "cliente";
    static final String NOME = "nome";
    static final String COGNOME = "cognome";
    static final String TESSERA = "tessera";
    static final String CARTA_ID = "cartaId";

    public ClienteItem() { }
    public ClienteItem(String cid, String n, String c, String t) {
        this.cartaId = cid;
        this.nome = n;
        this.cognome = c;
        this.tessera = t;
    }
    public void setCartaId(String cartaId) {this.cartaId = cartaId;}
    public void setCognome(String cognome) {this.cognome = cognome;}
    public void setNome(String nome) {this.nome = nome;}
    public void setTessera(String tessera) {this.tessera = tessera;}
    public String getTessera() {return tessera;}
    public String getCartaId() {return cartaId;}
    public String getCognome() {return cognome;}
    public String getNome() {return nome;}
}
