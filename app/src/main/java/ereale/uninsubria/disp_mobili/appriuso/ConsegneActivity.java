package ereale.uninsubria.disp_mobili.appriuso;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.EventListener;
import java.util.TreeMap;

import static ereale.uninsubria.disp_mobili.appriuso.NetworkStateChangeReceiver.IS_NETWORK_AVAILABLE;

/*
public class ConsegneActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private static final String TAG = "ConsegneActivity";

    private RecyclerView mRecyclerView;
    private RecyclerAdapterConsegne mAdapter;
    private FirebaseDatabase database;
    private TreeMap<Integer, ConsegneItem> consegneItems;
    private Intent mIntent;
    private DrawerLayout drawer;
    int idCliente;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consegne);
        Toolbar toolbar = findViewById(R.id.tool_bar); //inserimento toolbar
        setSupportActionBar(toolbar);

        if(!isConnected(ConsegneActivity.this)) { //internet attivo
            final Snackbar snackBar = Snackbar.make(findViewById(R.id.drawer_layout), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE); //messaggio a comparsa in basso allo schermo
            snackBar.setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ConsegneActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
            snackBar.show();
        }
        IntentFilter intentFilter = new IntentFilter(NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION); //intent implicito: mi scegliera  la NetworkStateChangeReceiver perchè gli passo NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {   //assistente per la creazione di Intent
            @Override
            public void onReceive(Context context, Intent intent) { //override di NetworkStateChangeReceiver perchè extends BroadcastReceiver
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                if(!isNetworkAvailable) {
                    final Snackbar snackBar = Snackbar.make(findViewById(R.id.drawer_layout), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE);
                    snackBar.setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ConsegneActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    });
                    snackBar.show();
                }
            }
        }, intentFilter);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        //questo è il punto
        if (mFirebaseUser == null) { //accesso con mail
            Intent intent = new Intent(this, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        drawer=findViewById(R.id.drawer_layout); //menu laterale
        NavigationView navigationView=findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this,drawer,toolbar,R.string.open,R.string.close); //per mettere le tre lineettte
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //TextView emailTxt=findViewById(R.id.txtEmail);
        //if(mFirebaseUser!=null)
        //emailTxt.setText(mFirebaseUser.getEmail());

        //Riferimento db
        database = FirebaseDatabase.getInstance();

        //the array list containing the Consegne items: ogni istanza presente nel db
        consegneItems = new TreeMap<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mAdapter = new RecyclerAdapterConsegne(consegneItems);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        database.getReference(ConsegneItem.CONSEGNA).addChildEventListener(consegneListener);


        FloatingActionButton fab = findViewById(R.id.fab);
        mIntent = new Intent(this, NewConsegneActivity.class);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(mIntent,1);
            }
        });

    }

    private ChildEventListener consegneListener = new ChildEventListener() {
        @Override
        public synchronized void onChildAdded(DataSnapshot dataSnapshot, String s) {
            if(dataSnapshot.getValue()!=null) {
                Log.d(TAG,dataSnapshot+"");
                getConsegnaItemObject(dataSnapshot,Integer.parseInt(dataSnapshot.getKey()));

            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
            if(dataSnapshot.getValue()!=null) {
                Log.d(TAG,dataSnapshot+"");
                getConsegnaItemObject(dataSnapshot,Integer.parseInt(dataSnapshot.getKey()));

            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            if(dataSnapshot.getValue()!=null) {

                        consegneItems.remove(Integer.parseInt(dataSnapshot.getKey()));
                        mAdapter.notifyDataSetChanged();
                        mAdapter.notifyItemRemoved(Integer.parseInt(dataSnapshot.getKey()));
                        mAdapter.updateFullList(consegneItems);
                        //Toast.makeText(getApplicationContext(), "consegna rimossa", Toast.LENGTH_LONG).show(); //non lo faccio fare perchè me lo mostra anche quando elimino una istanza nel db per la modifica (Del codice)
                        return;

            }else{
                Toast.makeText(getApplicationContext(), "consegna non rimossa", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
        @Override
        public void onCancelled(DatabaseError databaseError) {}

    };

    @Override
    public boolean onCreateOptionsMenu(Menu miomenu) { //ricerca
        getMenuInflater().inflate(R.menu.menu, miomenu);

        MenuItem searchItem = miomenu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.action_logout){
            AlertDialog.Builder alert = new AlertDialog.Builder(ConsegneActivity.this);
            alert.setMessage("Sei sicuro di voler effettuare il logout?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FirebaseAuth.getInstance().signOut();
                    mFirebaseUser = null;
                    Intent intent = new Intent(ConsegneActivity.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });
            alert.create().show();
        }

        return true;
    }

    private void getConsegnaItemObject(DataSnapshot dataSnapshot,final int ind){  //creazione istanza
        final String cod = dataSnapshot.child(ConsegneItem.CODICE).getValue().toString();
        Log.d(TAG,"Chiave "+cod);
        final int codCli = Integer.parseInt(dataSnapshot.child(ConsegneItem.CLIENTE).getValue().toString());
        Log.d(TAG,"Cliente "+codCli);
        final int categoria = Integer.parseInt(dataSnapshot.child(ConsegneItem.CATEGORIA).getValue().toString());
        Log.d(TAG,"Categoria "+categoria);
        final String data;
        data = dataSnapshot.child(ConsegneItem.DATA).getValue().toString();
        Log.d(TAG,"Data "+data);
        final String descrizione = dataSnapshot.child(ConsegneItem.DESCRIZIONE).getValue().toString();
        Log.d(TAG,"Descrizione "+descrizione);
        final String magazzino=dataSnapshot.child(ConsegneItem.MAGAZZINO).getValue().toString();
        Log.d(TAG,"Magazzino "+magazzino);
        final String fascia=dataSnapshot.child(ConsegneItem.FASCIA).getValue().toString();
        Log.d(TAG,"Fascia "+fascia);
        final boolean isMagazzino=magazzino.equals("true");
        Log.d(TAG,"Magazzino "+isMagazzino);
        final String ritirata=dataSnapshot.child(ConsegneItem.RITIRATA).getValue().toString();
        final boolean rit=ritirata.equals("true");
        Log.d(TAG,"Ritirata"+rit);

        final ConsegneItem c=new ConsegneItem(codCli, descrizione, categoria,data,isMagazzino,fascia);

        DatabaseReference myRef = database.getReference(ClienteItem.CLIENTE);
        Query query = myRef.orderByKey().equalTo(""+codCli);
        query.addChildEventListener(new ChildEventListener() { //per determinare nome/cognome dato l'id del cliente
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot1, @Nullable String s) {
                String nome=dataSnapshot1.child(ClienteItem.NOME).getValue().toString()+" "+dataSnapshot1.child(ClienteItem.COGNOME).getValue().toString();

                c.decidiNomeCliente(nome);
                c.impostaRitirata(rit);
                c.setCodice(cod);
                Log.d("CODICE",cod+" "+nome);
                consegneItems.put(ind,c);
                mAdapter.notifyDataSetChanged();
                mAdapter.notifyItemInserted(ind);
                mAdapter.updateFullList(consegneItems);
                Log.d(TAG,"Istanza registrata "+c);
                Log.d(TAG,"Istanza registrata "+c.toString());
            }
            @Override public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
            @Override public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
        } );

    }

    @Override
    public void onBackPressed(){
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item){
        switch (item.getItemId()){
            case R.id.consegne:
                Intent intent=new Intent(this,ConsegneActivity.class);
                this.startActivity(intent);
                break;
            case R.id.ritiri:
                Intent intent1=new Intent(this,RitiroActivity.class);
                this.startActivity(intent1);
                break;
            case R.id.home:
                Intent intent2=new Intent(this,MainActivity.class);
                this.startActivity(intent2);
                break;
            case R.id.clienti:
                Intent intent3=new Intent(this,ClienteActivity.class);
                this.startActivity(intent3);
                break;
            case R.id.statistiche:
                Intent intent4=new Intent(this,Statistiche.class);
                this.startActivity(intent4);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else
            return false;
    }

}
*/


























public class ConsegneActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private static final String TAG = "ConsegneActivity";

    private RecyclerView mRecyclerView;
    private RecyclerAdapterConsegne mAdapter;
    private FirebaseDatabase database;
    private TreeMap<Integer, ConsegneItem> consegneItems;
    private Intent mIntent;
    private DrawerLayout drawer;
    int idCliente;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_consegne);
        Toolbar toolbar = findViewById(R.id.tool_bar); //inserimento toolbar
        setSupportActionBar(toolbar);

        if(!isConnected(ConsegneActivity.this)) { //internet attivo
            final Snackbar snackBar = Snackbar.make(findViewById(R.id.drawer_layout), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE); //messaggio a comparsa in basso allo schermo
            snackBar.setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ConsegneActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
            snackBar.show();
        }
        IntentFilter intentFilter = new IntentFilter(NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION); //intent implicito: mi scegliera  la NetworkStateChangeReceiver perchè gli passo NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {   //assistente per la creazione di Intent
            @Override
            public void onReceive(Context context, Intent intent) { //override di NetworkStateChangeReceiver perchè extends BroadcastReceiver
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                if(!isNetworkAvailable) {
                    final Snackbar snackBar = Snackbar.make(findViewById(R.id.drawer_layout), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE);
                    snackBar.setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(ConsegneActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    });
                    snackBar.show();
                }
            }
        }, intentFilter);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();


        //questo è il punto
        if (mFirebaseUser == null) { //accesso con mail
            Intent intent = new Intent(this, Login.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }

        drawer=findViewById(R.id.drawer_layout); //menu laterale
        NavigationView navigationView=findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this,drawer,toolbar,R.string.open,R.string.close); //per mettere le tre lineettte
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //TextView emailTxt=findViewById(R.id.txtEmail);
        //if(mFirebaseUser!=null)
        //emailTxt.setText(mFirebaseUser.getEmail());

        //Riferimento db
        database = FirebaseDatabase.getInstance();

        //the array list containing the Consegne items: ogni istanza presente nel db
        consegneItems = new TreeMap<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mAdapter = new RecyclerAdapterConsegne(consegneItems);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        database.getReference(ConsegneItem.CONSEGNA).addChildEventListener(consegneListener);


        FloatingActionButton fab = findViewById(R.id.fab);
        mIntent = new Intent(this, NewConsegneActivity.class);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(mIntent,1);
            }
        });

    }
    private ChildEventListener consegneListener = new ChildEventListener() {
        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String s) { //quando viene aggiunto un nuovo cliente
            if (dataSnapshot.getValue() != null) {
                /*String cid = dataSnapshot.child(ClienteItem.CARTA_ID).getValue().toString();
                String nome = dataSnapshot.child(ClienteItem.NOME).getValue().toString();
                String cognome = dataSnapshot.child(ClienteItem.COGNOME).getValue().toString();
                String tessera = dataSnapshot.child(ClienteItem.TESSERA).getValue().toString();
                int indice = Integer.parseInt(dataSnapshot.getKey());
                clienteItems.put(indice, new ClienteItem(cid, nome,cognome, tessera));
                mAdapter.notifyItemInserted(clienteItems.size());
                mAdapter.updateFullList(clienteItems);*/

                final int ind=Integer.parseInt(dataSnapshot.getKey());


                final String cod = dataSnapshot.child(ConsegneItem.CODICE).getValue().toString();
                Log.d(TAG,"Chiave "+cod);
                final int codCli = Integer.parseInt(dataSnapshot.child(ConsegneItem.CLIENTE).getValue().toString());
                Log.d(TAG,"Cliente "+codCli);
                final int categoria = Integer.parseInt(dataSnapshot.child(ConsegneItem.CATEGORIA).getValue().toString());
                Log.d(TAG,"Categoria "+categoria);
                final String data;
                data = dataSnapshot.child(ConsegneItem.DATA).getValue().toString();
                Log.d(TAG,"Data "+data);
                final String descrizione = dataSnapshot.child(ConsegneItem.DESCRIZIONE).getValue().toString();
                Log.d(TAG,"Descrizione "+descrizione);
                final String magazzino=dataSnapshot.child(ConsegneItem.MAGAZZINO).getValue().toString();
                Log.d(TAG,"Magazzino "+magazzino);
                final String fascia=dataSnapshot.child(ConsegneItem.FASCIA).getValue().toString();
                Log.d(TAG,"Fascia "+fascia);
                final boolean isMagazzino=magazzino.equals("true");
                Log.d(TAG,"Magazzino "+isMagazzino);
                final String ritirata=dataSnapshot.child(ConsegneItem.RITIRATA).getValue().toString();
                final boolean rit=ritirata.equals("true");
                Log.d(TAG,"Ritirata"+rit);

                final ConsegneItem c=new ConsegneItem(codCli, descrizione, categoria,data,isMagazzino,fascia);

                DatabaseReference myRef = database.getReference(ClienteItem.CLIENTE);
                Query query = myRef.orderByKey().equalTo(""+codCli);
                query.addChildEventListener(new ChildEventListener() { //per determinare nome/cognome dato l'id del cliente
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot1, @Nullable String s) {
                        String nome=dataSnapshot1.child(ClienteItem.NOME).getValue().toString()+" "+dataSnapshot1.child(ClienteItem.COGNOME).getValue().toString();

                        c.decidiNomeCliente(nome);
                        c.impostaRitirata(rit);
                        c.setCodice(cod);
                        Log.d("CODICE",cod+" "+nome);
                        consegneItems.put(ind,c);
                        mAdapter.notifyDataSetChanged();
                        mAdapter.notifyItemInserted(ind);
                        mAdapter.updateFullList(consegneItems);
                        Log.d(TAG,"Istanza registrata "+c);
                        Log.d(TAG,"Istanza registrata "+c.toString());
                    }
                    @Override public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                    @Override public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
                    @Override public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                    @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                } );
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String s) { //quando viene modificato un cliente
            if (dataSnapshot.getValue() != null) {
                /*String cid = dataSnapshot.child(ClienteItem.CARTA_ID).getValue().toString();
                String nome = dataSnapshot.child(ClienteItem.NOME).getValue().toString();
                String cognome = dataSnapshot.child(ClienteItem.COGNOME).getValue().toString();
                String tessera = dataSnapshot.child(ClienteItem.TESSERA).getValue().toString();
                int indice = Integer.parseInt(dataSnapshot.getKey());
                clienteItems.put(indice, new ClienteItem(cid, nome, cognome, tessera));
                mAdapter.notifyDataSetChanged();
                mAdapter.updateFullList(clienteItems);*/

                final int ind=Integer.parseInt(dataSnapshot.getKey());


                final String cod = dataSnapshot.child(ConsegneItem.CODICE).getValue().toString();
                Log.d(TAG,"Chiave "+cod);
                final int codCli = Integer.parseInt(dataSnapshot.child(ConsegneItem.CLIENTE).getValue().toString());
                Log.d(TAG,"Cliente "+codCli);
                final int categoria = Integer.parseInt(dataSnapshot.child(ConsegneItem.CATEGORIA).getValue().toString());
                Log.d(TAG,"Categoria "+categoria);
                final String data;
                data = dataSnapshot.child(ConsegneItem.DATA).getValue().toString();
                Log.d(TAG,"Data "+data);
                final String descrizione = dataSnapshot.child(ConsegneItem.DESCRIZIONE).getValue().toString();
                Log.d(TAG,"Descrizione "+descrizione);
                final String magazzino=dataSnapshot.child(ConsegneItem.MAGAZZINO).getValue().toString();
                Log.d(TAG,"Magazzino "+magazzino);
                final String fascia=dataSnapshot.child(ConsegneItem.FASCIA).getValue().toString();
                Log.d(TAG,"Fascia "+fascia);
                final boolean isMagazzino=magazzino.equals("true");
                Log.d(TAG,"Magazzino "+isMagazzino);
                final String ritirata=dataSnapshot.child(ConsegneItem.RITIRATA).getValue().toString();
                final boolean rit=ritirata.equals("true");
                Log.d(TAG,"Ritirata"+rit);

                final ConsegneItem c=new ConsegneItem(codCli, descrizione, categoria,data,isMagazzino,fascia);

                DatabaseReference myRef = database.getReference(ClienteItem.CLIENTE);
                Query query = myRef.orderByKey().equalTo(""+codCli);
                query.addChildEventListener(new ChildEventListener() { //per determinare nome/cognome dato l'id del cliente
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot1, @Nullable String s) {
                        String nome=dataSnapshot1.child(ClienteItem.NOME).getValue().toString()+" "+dataSnapshot1.child(ClienteItem.COGNOME).getValue().toString();

                        c.decidiNomeCliente(nome);
                        c.impostaRitirata(rit);
                        c.setCodice(cod);
                        Log.d("CODICE",cod+" "+nome);
                        consegneItems.put(ind,c);
                        mAdapter.notifyDataSetChanged();
                        mAdapter.notifyItemInserted(ind);
                        mAdapter.updateFullList(consegneItems);
                        Log.d(TAG,"Istanza registrata "+c);
                        Log.d(TAG,"Istanza registrata "+c.toString());
                    }
                    @Override public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                    @Override public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
                    @Override public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                    @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                } );
            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) { //quando viene rimosso un cliente
            if (dataSnapshot.getValue() != null) {
                consegneItems.remove(Integer.parseInt(dataSnapshot.getKey()));
                mAdapter.notifyDataSetChanged();
                mAdapter.notifyItemRemoved(Integer.parseInt(dataSnapshot.getKey()));
                mAdapter.updateFullList(consegneItems);
                //Toast.makeText(getApplicationContext(), "consegna rimossa", Toast.LENGTH_LONG).show(); //non lo faccio fare perchè me lo mostra anche quando elimino una istanza nel db per la modifica (Del codice)
                return;

            }else{
                Toast.makeText(getApplicationContext(), "consegna non rimossa", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
        @Override
        public void onCancelled(DatabaseError databaseError) {}
    };
    @Override
    public boolean onCreateOptionsMenu(Menu miomenu) {
        /*getMenuInflater().inflate(R.menu.menu, miomenu);

        MenuItem searchItem = miomenu.findItem(R.id.action_search); //ricerca
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText); //modalità di ricerca
                return false;
            }
        });
        return true;*/


        getMenuInflater().inflate(R.menu.menu, miomenu);

        MenuItem searchItem = miomenu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        /*int id = item.getItemId();
        if(id == R.id.action_logout){ //pulsante di logout
            AlertDialog.Builder alert = new AlertDialog.Builder(ClienteActivity.this);
            alert.setTitle("Logout");
            alert.setMessage("Sei sicuro di voler effettuare il logout?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FirebaseAuth.getInstance().signOut();
                    mFirebaseUser = null;
                    Intent intent = new Intent(ClienteActivity.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });
            alert.create().show();
        }
        return true;*/

        int id = item.getItemId();
        if(id == R.id.action_logout){
            AlertDialog.Builder alert = new AlertDialog.Builder(ConsegneActivity.this);
            alert.setMessage("Sei sicuro di voler effettuare il logout?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    FirebaseAuth.getInstance().signOut();
                    mFirebaseUser = null;
                    Intent intent = new Intent(ConsegneActivity.this, Login.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {}
            });
            alert.create().show();
        }

        return true;

    }

    @Override
    public void onBackPressed(){
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item){ //cosa succede se schiaccio una voce del menu laterale
        switch (item.getItemId()){
            case R.id.consegne:
                Intent intent=new Intent(this,ConsegneActivity.class);
                this.startActivity(intent);
                break;
            case R.id.ritiri:
                Intent intent1=new Intent(this,RitiroActivity.class);
                this.startActivity(intent1);
                break;
            case R.id.home:
                Intent intent2=new Intent(this,MainActivity.class);
                this.startActivity(intent2);
                break;
            case R.id.clienti:
                Intent intent3=new Intent(this,ClienteActivity.class);
                this.startActivity(intent3);
                break;
            case R.id.statistiche:
                Intent intent4=new Intent(this,Statistiche.class);
                this.startActivity(intent4);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public boolean isConnected(Context context) { //verifica presenza di connessione

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else
            return false;
    }
}
