package ereale.uninsubria.disp_mobili.appriuso;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import static ereale.uninsubria.disp_mobili.appriuso.NetworkStateChangeReceiver.IS_NETWORK_AVAILABLE;

public class GraficoGuadagni extends AppCompatActivity{
    private Button buttonMesi, buttonAnni;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            setContentView(R.layout.grafico_guadagni);
        }else if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.grafico_guadagni1);
        }
        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(!isConnected(GraficoGuadagni.this)) {
            final Snackbar snackBar = Snackbar.make(findViewById(R.id.layoutGrafico), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE);
            snackBar.setAction("Ok", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(GraficoGuadagni.this, Statistiche.class);
                    startActivity(intent);
                }
            });
            snackBar.show();
        }
        IntentFilter intentFilter = new IntentFilter(NetworkStateChangeReceiver.NETWORK_AVAILABLE_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean isNetworkAvailable = intent.getBooleanExtra(IS_NETWORK_AVAILABLE, false);
                if(!isNetworkAvailable) {
                    final Snackbar snackBar = Snackbar.make(findViewById(R.id.layoutGrafico), "Assenza di connessione", Snackbar.LENGTH_INDEFINITE);
                    snackBar.setAction("Ok", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(GraficoGuadagni.this, Statistiche.class);
                            startActivity(intent);
                        }
                    });
                    snackBar.show();
                }
            }
        }, intentFilter);

        buttonMesi = findViewById(R.id.buttonMesi);
        buttonAnni = findViewById(R.id.buttonAnni);

        if (getSupportFragmentManager().findFragmentByTag("anno_fragment") == null && getSupportFragmentManager().findFragmentByTag("mese_fragment") == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GuadagniAnniFragment(), "anno_fragment").commit();
            buttonAnni.setEnabled(false);
            buttonMesi.setEnabled(true);
        }else if(getSupportFragmentManager().findFragmentByTag("anno_fragment") != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GuadagniAnniFragment(), "anno_fragment").commit();
            buttonAnni.setEnabled(false);
            buttonMesi.setEnabled(true);
        }else if(getSupportFragmentManager().findFragmentByTag("mese_fragment") != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GuadagniMesiFragment(), "mese_fragment").commit();
            buttonAnni.setEnabled(true);
            buttonMesi.setEnabled(false);
        }

        buttonMesi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GuadagniMesiFragment(),"mese_fragment").commit();
                buttonAnni.setEnabled(true);
                buttonMesi.setEnabled(false);
            }
        });
        buttonAnni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,  new GuadagniAnniFragment(),"anno_fragment").commit();
                buttonAnni.setEnabled(false);
                buttonMesi.setEnabled(true);
            }
        });
    }
    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else
            return false;
    }
}