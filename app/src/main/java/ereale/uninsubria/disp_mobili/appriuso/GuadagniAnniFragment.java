package ereale.uninsubria.disp_mobili.appriuso;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class GuadagniAnniFragment extends Fragment {
    private TextView TxtMediaRitiriAnni,TxtTotaleGuadagni;
    private LineChart lineChart;
    private TreeMap<Integer, Float> ritiriAnni;
    private List<Integer> anni;
    private double mediaRit=0.0;
    private View view;
    private int min;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            view = inflater.inflate(R.layout.fragment_guadagni_anni, container, false);
        }else{
            view = inflater.inflate(R.layout.fragment_guadagni_anni1, container, false);
        }
        lineChart = view.findViewById(R.id.chart);

        TxtMediaRitiriAnni=view.findViewById(R.id.txtMediaRit);
        TxtTotaleGuadagni=view.findViewById(R.id.txtTot);

        ritiriAnni = new TreeMap<>(); //guadagni per ogni anno
        anni = new ArrayList<>();
        DatabaseReference myRefRit = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO);
        myRefRit.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null) {
                    min=Calendar.getInstance().get(Calendar.YEAR);
                    DatabaseReference myRefCons = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
                    myRefCons.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                            for(DataSnapshot d : dataSnapshot1.getChildren()) { //trovo l'anno minore tra le consegne (sicuramente non può esserci un ritiro prima di una consegna per quello l'anno minore lo cerco nelle consegne
                                StringTokenizer data = new StringTokenizer(d.child(ConsegneItem.DATA).getValue().toString(), "-");
                                data.nextToken();
                                data.nextToken();
                                int anno = Integer.parseInt(data.nextToken());
                                if(anno<min) min=anno;
                            }
                            riempiAnni(min);
                            for(DataSnapshot d : dataSnapshot.getChildren()) {
                                StringTokenizer data = new StringTokenizer(d.child(RitiroItem.DATA).getValue().toString(), "-");
                                data.nextToken(); data.nextToken();
                                int anno =Integer.parseInt(data.nextToken());
                                float c = ritiriAnni.get(anno); //nella posizione dell'anno (indice=anno) inserisco la somma delle offerte in quell'anno
                                double offerta = Double.parseDouble(d.child(RitiroItem.OFFERTA).getValue().toString());
                                c+=offerta;
                                ritiriAnni.put(anno, c);
                            }
                            Collections.sort(anni);
                            ArrayList<ILineDataSet> dataSet;
                            ArrayList<Entry> entryListGuadagni = new ArrayList<>();
                            for(int i=0;i<anni.size();i++){
                                mediaRit+=ritiriAnni.get(anni.get(i));
                                entryListGuadagni.add(new Entry(ritiriAnni.get(anni.get(i)),i));
                            }
                            TxtTotaleGuadagni.setText(new DecimalFormat("#.#").format(mediaRit)+"€");
                            mediaRit=mediaRit/anni.size();
                            TxtMediaRitiriAnni.setText(new DecimalFormat("#.###").format(mediaRit)+"€");
                            LineDataSet dataSetRitiri = new LineDataSet(entryListGuadagni,"Guadagni");
                            dataSetRitiri.setColor(view.getContext().getResources().getColor(R.color.colorGuadagno));
                            dataSetRitiri.setLineWidth(3f);
                            dataSetRitiri.setDrawCircleHole(false);
                            dataSetRitiri.setCircleColor(view.getContext().getResources().getColor(R.color.colorGuadagno));
                            dataSet = new ArrayList<>();
                            dataSet.add(dataSetRitiri);
                            ArrayList<String> anniS=new ArrayList<>();
                            for(int a : anni)
                                anniS.add(String.valueOf(a));
                            LineData lineData = new LineData(anniS,dataSet);
                            //lineData.setDrawValues(false);
                            lineChart.setData(lineData);
                            lineChart.getLegend().setEnabled(false);
                            lineChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
                            lineChart.animateY(2000);
                            lineChart.invalidate();
                            lineChart.setDescription("");
                            lineChart.getXAxis().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                            lineChart.getXAxis().setAxisLineWidth(1.3f);
                            lineChart.getXAxis().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                            lineChart.getAxisLeft().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                            lineChart.getAxisLeft().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                            lineChart.getAxisLeft().setAxisLineWidth(2f);
                            lineChart.getAxisLeft().setAxisMinValue(0f);
                            lineChart.getAxisRight().setTextColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                            lineChart.getAxisRight().setAxisLineColor(view.getContext().getResources().getColor(R.color.colorGrafico));
                            lineChart.getAxisRight().setAxisLineWidth(2f);
                            lineChart.getAxisRight().setAxisMinValue(0f);
                        }
                        @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                    });
                }
            }
            @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
        return view;
    }
    private void riempiAnni(int anno) { //inserisce tutti gli anni da il minimo passato come argomento a quello attuale
        for(int i=anno;i<=Calendar.getInstance().get(Calendar.YEAR);i++) {
            anni.add(i);
            ritiriAnni.put(i, 0f);
        }
    }
}