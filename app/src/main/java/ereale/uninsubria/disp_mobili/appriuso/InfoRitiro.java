package ereale.uninsubria.disp_mobili.appriuso;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.TreeMap;
import java.util.TreeSet;

public class InfoRitiro extends AppCompatActivity {
    private static final String TAG = "InfoRitiro";

    private String cliente, codice, consegna, data, offerta, descrizione="", categoria;
    private EditText txtOfferta;
    private TextView txtDescrizione;
    private Button salvaModifiche;
    private Intent resultIntent;
    Spinner consegnaSpinner;
    TreeMap<String, AdapterRitiroItem> consegne;
    ArrayAdapter<String> spinnerAdapterCons;
    FasciaItem fasciaAttuale;
    private ArrayAdapter<String> adapterCli;
    private AutoCompleteTextView TxtCliente;
    private TreeMap<String, Integer> clienti;
    private Double vInserito;
    int idCliente;

    /*calendario*/
    int year_x, month_x, day_x;
    static final int DIALOG_ID=0;
    private TextView txtData;
    private Button btn;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.info_ritiro);
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.info_ritiro1);
        }

        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        codice=intent.getStringExtra(RitiroItem.ID);

        data = intent.getStringExtra(RitiroItem.DATA);
        txtData = findViewById(R.id.txtData);

        /*calendario*/
        String[] d=data.split("-");
        final Calendar cal = Calendar.getInstance();
        year_x = Integer.parseInt(d[2]);
        month_x = Integer.parseInt(d[1])-1;
        day_x = Integer.parseInt(d[0]);
        showDialogOnButtonClick();

        txtData.setText(data);

        offerta=intent.getStringExtra(RitiroItem.OFFERTA);
        txtOfferta=findViewById(R.id.txtOfferta);
        txtOfferta.setText(offerta);

        fasciaAttuale = new FasciaItem();

        txtDescrizione=findViewById(R.id.editTxtDescrizione);

        consegna=intent.getStringExtra(RitiroItem.CONSEGNA); //ha ;
        consegnaSpinner=(Spinner)findViewById(R.id.spinnerConsegna);
        spinnerAdapterCons= new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item);
        leggiConsegne(intent.getStringExtra(RitiroItem.CONSEGNA));
        consegnaSpinner.setAdapter(spinnerAdapterCons);
        consegnaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Elemento selezionato: parent.getItemAtPosition(position).toString()
                String consegna = parent.getItemAtPosition(position).toString();
                Log.d(TAG,consegna);
                txtDescrizione.setText(consegne.get(consegna).getDescrizione());
                if(descrizione.equals(""))
                    descrizione=consegne.get(consegna).getDescrizione();
                categoria=consegne.get(consegna).getCategoria();
                fasciaAttuale = Fascia.getfasciaDB(consegne.get(consegna).getOfferta());
                txtOfferta.setHint(fasciaAttuale.getNomeCompleto());
            }
            @Override public void onNothingSelected(AdapterView<?> arg0) {}
        });
        //consegne è settato in leggi consegne

        leggiClienti();
        TxtCliente = findViewById(R.id.editTxtCliente);
        adapterCli = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        TxtCliente.setAdapter(adapterCli);
        TxtCliente.setText(intent.getStringExtra(RitiroItem.CLIENTE));
        cliente = TxtCliente.getText().toString();

        Button registraCliente = findViewById(R.id.buttonAggiungiCliente);
        registraCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cli = new Intent(InfoRitiro.this, NewCliente.class);
                cli.putExtra("chiamante","InfoRitiro" );
                startActivityForResult(cli, 2);
            }
        });

        resultIntent = new Intent();

        salvaModifiche = findViewById(R.id.buttonModifiche);
        salvaModifiche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(clienti.containsKey(TxtCliente.getText().toString())) {
                    //non controllo se i campi sono vuoti perchè li inserisco io all'inizio
                    try {
                        String val = txtOfferta.getText().toString();
                        vInserito = Double.parseDouble(val.replace(",", "."));
                        if (!(vInserito >= fasciaAttuale.getMinimo() && (vInserito <= fasciaAttuale.getMassimo() || fasciaAttuale.getMassimo() == 0))) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(InfoRitiro.this);
                            alert.setTitle("Vuoi procedere col salvataggio?");
                            alert.setMessage("Il prodotto appartiene alla fascia: " + fasciaAttuale.getNomeCompleto());
                            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    animaIntent();
                                }
                            });
                            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            alert.create().show();
                        } else {
                            animaIntent();
                        }

                    } catch (Exception e) {
                        Log.d(TAG, e.getMessage());
                        Toast.makeText(getApplicationContext(), "L'offerta deve essere un valore numerico", Toast.LENGTH_LONG).show();
                    }
                }else{
                    AlertDialog.Builder alert = new AlertDialog.Builder(InfoRitiro.this);
                    alert.setTitle("Nuovo cliente");
                    alert.setMessage("Il cliente " + TxtCliente.getText().toString() + " non è registrato. Vuoi aggiungerlo?");
                    alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent cli = new Intent(InfoRitiro.this, NewCliente.class);
                            cli.putExtra("chiamante", "NewConsegneActivity");
                            startActivityForResult(cli, 2);
                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alert.create().show();
                }

            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem item) { //freccia indietro
        backSalvareModifiche();
        return true;
    }

    public void animaIntent() {
        resultIntent.putExtra(RitiroItem.CLIENTE, TxtCliente.getText().toString());
        resultIntent.putExtra(RitiroItem.ID, codice);
        String c = consegnaSpinner.getSelectedItem().toString();
        resultIntent.putExtra(RitiroItem.CONSEGNA, c);
        resultIntent.putExtra(RitiroItem.DATA, txtData.getText().toString());
        resultIntent.putExtra(RitiroItem.OFFERTA, txtOfferta.getText().toString());
        resultIntent.putExtra(ConsegneItem.CATEGORIA, categoria);
        Log.d(TAG, c);

        if (consegne.get(c).nomeCliente().equals(TxtCliente.getText().toString())) {
            AlertDialog.Builder alert = new AlertDialog.Builder(InfoRitiro.this);
            alert.setTitle("Sei sicuro di comprare?");
            String message = TxtCliente.getText().toString() + " ha effettuato la consegna: " + consegnaSpinner.getSelectedItem().toString();
            alert.setMessage(message);
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setResult(RESULT_OK, resultIntent);
                    finish();
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            alert.create().show();
        } else {
            setResult(RESULT_OK, resultIntent);
            finish();
        }


    }

    private void leggiConsegne(final String cons){ //cons ha ;
        consegne = new TreeMap<>();
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        Query query = myRef.orderByChild(ConsegneItem.RITIRATA).equalTo(false);
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if (dataSnapshot.exists()) {
                    final AdapterRitiroItem app = new AdapterRitiroItem();
                    app.setDescrizione(dataSnapshot.child(ConsegneItem.DESCRIZIONE).getValue().toString());
                    app.setCategoria(dataSnapshot.child(ConsegneItem.CATEGORIA).getValue().toString());
                    app.setOfferta(dataSnapshot.child(ConsegneItem.FASCIA).getValue().toString());
                    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
                    Query query = myRef.orderByKey().equalTo(dataSnapshot.child(ConsegneItem.CLIENTE).getValue().toString());
                    query.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                            String nome = dataSnapshot.child(ClienteItem.NOME).getValue().toString() + " " + dataSnapshot.child(ClienteItem.COGNOME).getValue().toString();
                            app.decidiNomeCliente(nome);
                        }
                        @Override public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                        @Override public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
                        @Override public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                        @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                    } );

                    consegne.put(dataSnapshot.child(ConsegneItem.CODICE).getValue().toString(), app);
                    spinnerAdapterCons.add(dataSnapshot.child(ConsegneItem.CODICE).getValue().toString());

                }
            }
            @Override public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
            @Override public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override public void onCancelled(DatabaseError databaseError) {}
        });
        Query query1=myRef.orderByChild(ConsegneItem.CODICE).equalTo(cons); //perchè se no in quello sopra non mi prende la consegna che sto considerando perchè non ha ritirata uguale a false
        query1.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if (dataSnapshot.exists()) {
                    final AdapterRitiroItem app = new AdapterRitiroItem();
                    app.setDescrizione(dataSnapshot.child(ConsegneItem.DESCRIZIONE).getValue().toString());
                    txtDescrizione.setText(dataSnapshot.child(ConsegneItem.DESCRIZIONE).getValue().toString());
                    app.setCategoria(dataSnapshot.child(ConsegneItem.CATEGORIA).getValue().toString());
                    categoria=dataSnapshot.child(ConsegneItem.CATEGORIA).getValue().toString();
                    app.setOfferta(dataSnapshot.child(ConsegneItem.FASCIA).getValue().toString());
                    fasciaAttuale = Fascia.getfasciaDB(dataSnapshot.child(ConsegneItem.FASCIA).getValue().toString());
                    txtOfferta.setHint(fasciaAttuale.getNomeCompleto());
                    DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
                    Query query = myRef.orderByKey().equalTo(dataSnapshot.child(ConsegneItem.CLIENTE).getValue().toString());
                    query.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                            String nome = dataSnapshot.child(ClienteItem.NOME).getValue().toString() + " " + dataSnapshot.child(ClienteItem.COGNOME).getValue().toString();
                            app.decidiNomeCliente(nome);
                        }
                        @Override public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                        @Override public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
                        @Override public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                        @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                    } );
                    consegne.put(dataSnapshot.child(ConsegneItem.CODICE).getValue().toString(), app);
                    spinnerAdapterCons.add(dataSnapshot.child(ConsegneItem.CODICE).getValue().toString());

                    consegnaSpinner.setSelection(spinnerAdapterCons.getPosition(dataSnapshot.child(ConsegneItem.CODICE).getValue().toString())); //per assegnare nuovo valore allo spinner
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    /*Per calendario*/
    public void showDialogOnButtonClick(){
        btn=(Button) findViewById(R.id.buttonCalendario);
        btn.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        showDialog(DIALOG_ID);
                    }
                }
        );
    }
    @Override
    protected Dialog onCreateDialog(int id){
        if(id == DIALOG_ID)
            return new DatePickerDialog(this, dpickerListener, year_x, month_x, day_x);
        return null;
    }
    private DatePickerDialog.OnDateSetListener dpickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            year_x = year;
            month_x = month+1;
            day_x = dayOfMonth;
            txtData.setText(day_x+"-"+month_x+"-"+year_x);
        }
    };

    @Override
    public void onBackPressed() {
        backSalvareModifiche();
    }
    private void backSalvareModifiche(){

        Log.d(TAG,"Offerta:"+txtOfferta.getText().toString());
        Log.d(TAG,"Descrizione:"+txtDescrizione.getText().toString());
        Log.d(TAG,"Data:"+txtData.getText().toString());
        Log.d(TAG,"Cliente:"+TxtCliente.getText().toString());
        if(!offerta.equals(txtOfferta.getText().toString()) || !descrizione.equals(txtDescrizione.getText().toString()) || !data.equals(txtData.getText().toString()) || !cliente.equals(TxtCliente.getText().toString()) ){   //per controllare consegna guardo la sua descrizione
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Vuoi salvare le modifiche?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if(clienti.containsKey(TxtCliente.getText().toString())) {
                        animaIntent();
                    }else {
                        AlertDialog.Builder alert = new AlertDialog.Builder(InfoRitiro.this);
                        alert.setTitle("Nuovo cliente");
                        alert.setMessage("Il cliente " + TxtCliente.getText().toString() + " non è registrato. Vuoi aggiungerlo?");
                        alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent cli = new Intent(InfoRitiro.this, NewCliente.class);
                                cli.putExtra("chiamante", "NewConsegneActivity");
                                startActivityForResult(cli, 2);
                            }
                        });
                        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        alert.create().show();
                    }
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setResult(RESULT_CANCELED, resultIntent);
                    finish();
                }
            });
            alert.create().show();
        }else{
            setResult(RESULT_CANCELED, resultIntent);
            finish();
        }
    }

    private void leggiClienti(){
        clienti = new TreeMap<>();
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
        Query query = myRef.orderByChild(ClienteItem.NOME);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String nc;
                    for(DataSnapshot d : dataSnapshot.getChildren()) {
                        nc="";
                        nc += d.child(ClienteItem.NOME).getValue().toString() + " " + d.child(ClienteItem.COGNOME).getValue().toString();
                        clienti.put(nc, Integer.parseInt(d.getKey()));
                    }
                    adapterCli.addAll(clienti.keySet());

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==2 && resultCode == RESULT_OK){ //cliente nuovo, chiamato con ImageButton
            String nome = data.getStringExtra(ClienteItem.NOME);
            String cognome = data.getStringExtra(ClienteItem.COGNOME);
            int indice = data.getIntExtra("indice", 1);
            clienti.put(nome+" "+cognome, indice);
            adapterCli.add(nome+" "+cognome);
            TxtCliente.setText(nome+" "+cognome);
        }
    }

}