package ereale.uninsubria.disp_mobili.appriuso;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import java.util.TreeMap;

public class RitiroCliente extends AppCompatActivity{
    private String nome, cognome, tessera, cid;
    private TreeMap<Integer, RitiroItem> ritiriItems;
    private RecyclerAdapterRitiriActivity mAdapter;
    private FirebaseDatabase database;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ritiri);
        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        nome = intent.getStringExtra(ClienteItem.NOME);
        cognome=intent.getStringExtra(ClienteItem.COGNOME);
        tessera=intent.getStringExtra(ClienteItem.TESSERA);
        cid=intent.getStringExtra(ClienteItem.CARTA_ID);
        int ind = intent.getIntExtra("indice",1);

        ritiriItems = new TreeMap<>();

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mAdapter = new RecyclerAdapterRitiriActivity(ritiriItems);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        database = FirebaseDatabase.getInstance();
        Query q = database.getReference(RitiroItem.RITIRO).orderByChild(RitiroItem.CLIENTE).equalTo(ind);
        q.addChildEventListener(new ChildEventListener() {
            @Override
            public synchronized void onChildAdded(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getValue()!=null) {
                    int idRitiro = Integer.parseInt(dataSnapshot.getKey());
                    ritiriItems.put(idRitiro, getRitiroItemObject(dataSnapshot));
                    aggiornaNomeCliente(dataSnapshot.child(RitiroItem.CLIENTE).getValue().toString(),idRitiro);
                }
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                if(dataSnapshot.getValue()!=null) {
                    int idRitiro = Integer.parseInt(dataSnapshot.getKey());
                    ritiriItems.put(idRitiro, getRitiroItemObject(dataSnapshot));
                    aggiornaNomeCliente(dataSnapshot.child(RitiroItem.CLIENTE).getValue().toString(),idRitiro);
                }
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue()!=null) {
                    int idRitiro = Integer.parseInt(dataSnapshot.getKey());
                    ritiriItems.remove(idRitiro);
                    mAdapter.notifyDataSetChanged();
                    mAdapter.updateFullList(ritiriItems);
                }
            }
            @Override public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
            @Override public void onCancelled(DatabaseError databaseError) {}
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.hide();
    }
    private RitiroItem getRitiroItemObject(DataSnapshot dataSnapshot){
        int cod = Integer.parseInt(dataSnapshot.getKey());
        String consegna=dataSnapshot.child(RitiroItem.CONSEGNA).getValue().toString();
        int cliente = Integer.parseInt(dataSnapshot.child(RitiroItem.CLIENTE).getValue().toString());
        String data = dataSnapshot.child(RitiroItem.DATA).getValue().toString();
        Double offerta= Double.parseDouble(dataSnapshot.child(RitiroItem.OFFERTA).getValue().toString());
        RitiroItem r=new RitiroItem(consegna,data,cliente,offerta);
        r.impostaID(cod);
        return r;
    }
    private void aggiornaNomeCliente(String idCliente,final int idRitiro){
        DatabaseReference myRef = database.getReference(ClienteItem.CLIENTE);
        Query query = myRef.orderByKey().equalTo(idCliente);
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String nome=dataSnapshot.child(ClienteItem.NOME).getValue().toString()+" "+dataSnapshot.child(ClienteItem.COGNOME).getValue().toString();
                RitiroItem r = ritiriItems.get(idRitiro);
                r.decidiNomeCliente(nome);
                ritiriItems.put(idRitiro, r);
                mAdapter.notifyDataSetChanged();
                mAdapter.updateFullList(ritiriItems);
            }
            @Override public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
            @Override public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
        } );
    }
    @Override
    public boolean onCreateOptionsMenu(Menu miomenu) {
        getMenuInflater().inflate(R.menu.menu_cerca, miomenu);

        MenuItem searchItem = miomenu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==android.R.id.home){
            Intent i = new Intent();
            i.putExtra(ClienteItem.NOME,nome);
            i.putExtra(ClienteItem.COGNOME,cognome);
            i.putExtra(ClienteItem.TESSERA,tessera);
            i.putExtra(ClienteItem.CARTA_ID,cid);
            setResult(RESULT_OK, i);
            finish();
        }
        return true;
    }
}
