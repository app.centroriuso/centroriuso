package ereale.uninsubria.disp_mobili.appriuso;
import android.widget.ImageView;
import java.util.Collection;
import java.util.TreeMap;

public class Categoria {
    private static TreeMap<String, String> list;
    public static Collection<String> creaSpinner(){
        list=new TreeMap<String,String>();
        list.put("1", "Oggetti da cucina");
        list.put("2","Elettrodomestici");
        list.put("3","Mobili");
        list.put("4","Oggetti sportivi");
        list.put("5","Oggetti di arredamento");
        list.put("6","Oggetti per l'infanzia");
        list.put("7","Giocattoli");
        list.put("8","Oggetti da bagno");
        list.put("9","Libri");
        list.put("10","CD/DVD/VHS");
        list.put("11","Oggetti per animali");
        list.put("12","Altro");
        return list.values();
    }


    public static int getID(String nome){
        for(int i=1;i<13;i++) {
            if (nome.equals(list.get("" + i + "")))
                return i;
        }
        return 0;
    }
    public static String getNome(String id){
        if(list==null)
            creaSpinner();
        return list.get(id);
    }
    public static String getIDfromImg(String name){
        String ret="";
        switch(name){
            case "categoria1_foreground":
                ret = "1";
            break;
            case "categoria2_foreground":
                ret = "2";
            break;
            case "categoria3_foreground":
                ret = "3";
            break;
            case "categoria4_foreground":
                ret = "4";
            break;
            case "categoria5_foreground":
                ret = "5";
            break;
            case "categoria6_foreground":
                ret = "6";
            break;
            case "categoria7_foreground":
                ret = "7";
            break;
            case "categoria8_foreground":
                ret = "12";
            break;
            case "categoria9_foreground":
                ret = "9";
                break;
            case "categoria10_foreground":
                ret = "10";
                break;
            case "categoria11_foreground":
                ret = "11";
                break;
            case "categoria12_foreground":
                ret = "8";
                break;
        }
        return ret;
    }
    public static void immagineCategoria(ImageView img, String id){
        switch(id){ //errore qui
            case "1":
            case "Oggetti da cucina":
                img.setImageResource(R.mipmap.categoria1_foreground);
            break;
            case "2":
            case "Mobili":
                img.setImageResource(R.mipmap.categoria2_foreground);
            break;
            case "3":
            case "Elettrodomestici":
                img.setImageResource(R.mipmap.categoria3_foreground);
            break;
            case "4":
            case "Oggetti sportivi":
                img.setImageResource(R.mipmap.categoria4_foreground);
            break;
            case "5":
            case "Oggetti di arredamento":
                img.setImageResource(R.mipmap.categoria5_foreground);
            break;
            case "6":
            case "Oggetti per l'infanzia":
                img.setImageResource(R.mipmap.categoria6_foreground);
            break;
            case "7":
            case "Giocattoli":
                img.setImageResource( R.mipmap.categoria7_foreground);
            break;
            case "8":
            case "Oggetti da bagno":
                img.setImageResource( R.mipmap.categoria12_foreground);
                break;
            case "9":
            case "Libri":
                img.setImageResource( R.mipmap.categoria9_foreground);
                break;
            case "10":
            case "CD/DVD/VHS":
                img.setImageResource( R.mipmap.categoria10_foreground);
                break;
            case "11":
            case "Oggetti per animali":
                img.setImageResource( R.mipmap.categoria11_foreground);
                break;
            case "12":
            case "Altro":
                img.setImageResource(R.mipmap.categoria8_foreground);
            break;
            default:
                img.setImageResource(R.mipmap.categoria_default_foreground);
            break;
        }
    }
}
