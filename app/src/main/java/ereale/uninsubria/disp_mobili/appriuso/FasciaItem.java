package ereale.uninsubria.disp_mobili.appriuso;

public class FasciaItem {
    private String nome;
    private double minimo;
    private double massimo;
    private String nomeCompleto;
    static final String FASCIA = "fasce_prezzo";
    static final String MIN = "min";
    static final String MAX = "max";
    public FasciaItem(){
        this.nome="";
        this.minimo=0.0;
        this.massimo=0.0;
        this.nomeCompleto="";
    }
    public FasciaItem(String nome, double minimo, double massimo) {
        this.nome = nome;
        this.minimo = minimo;
        this.massimo = massimo;
        this.nomeCompleto=nome+" (";
        if(this.massimo==0)
            this.nomeCompleto+="da "+minimo+"€)";
        else
            this.nomeCompleto+=minimo+"€ - "+massimo+"€)";
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getMinimo() {
        return minimo;
    }

    public void setMinimo(int minimo) {
        this.minimo = minimo;
    }

    public double getMassimo() {
        return massimo;
    }

    public void setMassimo(int massimo) {
        this.massimo = massimo;
    }
}
