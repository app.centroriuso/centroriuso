package ereale.uninsubria.disp_mobili.appriuso;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

public class InfoRitiroNoMod extends AppCompatActivity {
    private static final String TAG = "InfoRitiroNoMod";

    TextView txtCodice, txtConsegna, txtCliente, txtData, txtOfferta;
    String codice, consegna, cliente, data, offerta, activityChiamante, categoria;
    private Intent intentOpen;
    int idCliente;
    ImageView imgCategoria;
    String ritirata="Ritirato";

    public static final int MOD_RIT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.info_ritiro_nomod);
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setContentView(R.layout.info_ritiro_nomod1);
        }

        Toolbar myToolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txtCodice = findViewById(R.id.editTxtCodice);
        txtCliente = findViewById(R.id.editTxtCliente);
        txtConsegna = findViewById(R.id.editTxtConsegna);
        txtData = findViewById(R.id.editTxtData);
        txtOfferta = findViewById(R.id.editTxtOfferta);
        imgCategoria=findViewById(R.id.imageView2);

        Intent intent = getIntent();
        activityChiamante=intent.getStringExtra("chiamante");
        if(activityChiamante.equals("RitiriActivity")) {

            codice = intent.getStringExtra(RitiroItem.ID);

            cliente = intent.getStringExtra(RitiroItem.CLIENTE);

            consegna = intent.getStringExtra(RitiroItem.CONSEGNA);

            data = intent.getStringExtra(RitiroItem.DATA);

            offerta = intent.getStringExtra(RitiroItem.OFFERTA);

            categoria=intent.getStringExtra(ConsegneItem.CATEGORIA);

            txtCodice.setText(codice);
            txtCliente.setText(cliente);
            txtConsegna.setText(consegna);
            txtData.setText(data);
            txtOfferta.setText(offerta);
            Categoria.immagineCategoria(imgCategoria,categoria);


        }else{
            if(activityChiamante.equals("InfoConsNoMod")){ //devo fare tutte le query una dentro l'altra perchè se no non me le aggiorna in tempo reale
                Categoria.immagineCategoria(imgCategoria,intent.getStringExtra(ConsegneItem.CATEGORIA));

                DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO);
                Query query=myRef.orderByChild(RitiroItem.CONSEGNA).equalTo(intent.getStringExtra(ConsegneItem.CODICE));
                query.addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        codice = dataSnapshot.getKey();
                        idCliente = Integer.parseInt(dataSnapshot.child(RitiroItem.CLIENTE).getValue().toString());
                        consegna = dataSnapshot.child(RitiroItem.CONSEGNA).getValue().toString();
                        data = dataSnapshot.child(RitiroItem.DATA).getValue().toString();
                        offerta = dataSnapshot.child(RitiroItem.OFFERTA).getValue().toString();

                        txtCodice.setText(codice);
                        txtConsegna.setText(consegna);
                        txtData.setText(data);
                        txtOfferta.setText(offerta);

                        DatabaseReference myRef2 = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
                        Query query2 = myRef2.orderByKey().equalTo(idCliente+"");
                        query2.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                cliente=dataSnapshot.child(ClienteItem.NOME).getValue().toString()+" "+dataSnapshot.child(ClienteItem.COGNOME).getValue().toString();
                                Log.d(TAG,cliente);
                                txtCliente.setText(cliente);
                            }
                            @Override public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                            @Override public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
                            @Override public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                            @Override public void onCancelled(@NonNull DatabaseError databaseError) {}
                        } );

                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {}
                });
            }
        }

        Button modificaRitiro = findViewById(R.id.buttonModifica);
        modificaRitiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intentOpen = new Intent(InfoRitiroNoMod.this, InfoRitiro.class);
                intentOpen.putExtra(RitiroItem.ID,txtCodice.getText().toString());
                intentOpen.putExtra(RitiroItem.CONSEGNA,txtConsegna.getText().toString());
                intentOpen.putExtra(RitiroItem.CLIENTE,txtCliente.getText().toString());
                intentOpen.putExtra(RitiroItem.DATA,txtData.getText().toString());
                intentOpen.putExtra(RitiroItem.OFFERTA,txtOfferta.getText().toString());
                intentOpen.putExtra(ConsegneItem.CATEGORIA,categoria);
                startActivityForResult(intentOpen, MOD_RIT);
            }
        });

        Button eliminaRitiro = findViewById(R.id.buttonElimina);
        eliminaRitiro.setOnClickListener(new View.OnClickListener() {
            @Override
            public synchronized void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(InfoRitiroNoMod.this);
                alert.setTitle("Eliminazione ritiro");
                alert.setMessage("Sei sicuro di voler eliminare il ritiro "+txtCodice.getText().toString()+" con consegna numero "+txtConsegna.getText().toString()+" dall'elenco dei ritiri?");
                alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO+"/"+txtCodice.getText().toString()).removeValue();
                        Toast.makeText(getApplicationContext(), "ritiro rimosso", Toast.LENGTH_LONG).show();
                        settaConsegna();
                        finish();
                    }
                });
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {}
                });
                alert.create().show();
            }
        });

    }

    private synchronized void settaConsegna(){ //per redere ritirata=false in consegna dopo eliminazione di un ritiro
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        Query query=myRef.orderByChild(ConsegneItem.CODICE).equalTo(txtConsegna.getText().toString());
        query.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                String codice;
                Log.d(TAG,dataSnapshot+"");
                codice=dataSnapshot.getKey();
                Log.d(TAG,codice);

                DatabaseReference refCon =FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA+"/"+codice+"/"+ConsegneItem.RITIRATA);
                refCon.setValue(false);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_CANCELED) {
            txtCodice.setText(codice);
            txtConsegna.setText(consegna);
            txtCliente.setText(cliente);
            txtData.setText(this.data);
            txtOfferta.setText(offerta);
        }else if(resultCode == RESULT_OK){
            txtCodice.setText(data.getStringExtra(RitiroItem.ID));
            txtConsegna.setText(data.getStringExtra(RitiroItem.CONSEGNA));
            txtCliente.setText(data.getStringExtra(RitiroItem.CLIENTE));
            cliente=txtCliente.getText().toString();
            txtData.setText(data.getStringExtra(RitiroItem.DATA));
            this.data=txtData.getText().toString();
            txtOfferta.setText(data.getStringExtra(RitiroItem.OFFERTA));
            offerta=txtOfferta.getText().toString();
            categoria=data.getStringExtra(ConsegneItem.CATEGORIA);
            Categoria.immagineCategoria(imgCategoria,categoria);
            if(requestCode == MOD_RIT){
                if(!(consegna.equals(txtConsegna.getText().toString())))
                    aggiornaConsegna();
                consegna=txtConsegna.getText().toString();
                RitiroItem returnValue = new RitiroItem(consegna, this.data, idCliente,  Double.parseDouble(offerta));
                daiChiave(cliente, returnValue);

            }
        }
    }

    private synchronized void aggiornaConsegna(){ //se modifico il ritiro completamente, cambiando anche la consegna

        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        Query query=myRef.orderByChild(ConsegneItem.CODICE).equalTo(consegna);
        query.addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                            String codice;
                                            Log.d(TAG,dataSnapshot+"");
                                            codice=dataSnapshot.getKey();
                                            Log.d(TAG,codice);
                                            DatabaseReference refCon = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + codice + "/" + ConsegneItem.RITIRATA);
                                            refCon.setValue(false); //consegna precedente ancora disponibile

                                        }

                                        @Override
                                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                                        @Override
                                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}

                                        @Override
                                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {}
                                    });

        DatabaseReference myRef1 = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
        Query query1=myRef1.orderByChild(ConsegneItem.CODICE).equalTo(txtConsegna.getText().toString());
        query1.addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                            String codice;
                                            Log.d(TAG,dataSnapshot+"");
                                            codice=dataSnapshot.getKey();
                                            Log.d(TAG,codice);
                                            DatabaseReference refCon = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + codice + "/" + ConsegneItem.RITIRATA);
                                            refCon.setValue(true); //nuova consegna ritirata
                                        }

                                        @Override
                                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                                        @Override
                                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {}

                                        @Override
                                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {}

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {}
                                    });


        if(activityChiamante.equals("InfoConsNoMod")) //serve per comunicare se la consegna chiamante è stata effettivamente ritirata o no
            ritirata="Non ritirato";

    }

    private synchronized void aggiungiDB(RitiroItem r){
        FirebaseDatabase.getInstance().getReference(RitiroItem.RITIRO + "/" + r.prendiID()).setValue(r);
    }

    private void daiChiave(final String cliente, final RitiroItem returnValue){
        final String[] nome=cliente.split(" ");
        Log.d(TAG,"Nome:"+nome[0]+"Cognome:"+nome[1]);
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
        Log.d(TAG,myRef+"");
        Query query = myRef.orderByChild(ClienteItem.COGNOME).equalTo(nome[1]);
        Log.d(TAG,""+query);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    Log.d(TAG,""+d);
                    Log.d(TAG,d.child(ClienteItem.NOME)+"");
                    if ((d.child(ClienteItem.NOME).getValue().toString()).equals(nome[0])) {
                        idCliente = Integer.parseInt(d.getKey());
                        returnValue.setCliente(Integer.parseInt(d.getKey()));
                        returnValue.impostaID(Integer.parseInt(codice));
                        aggiungiDB(returnValue);
                        Log.d(TAG,idCliente+"");
                        return;
                    }
                    Log.d(TAG,idCliente+"");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}

        });


    }

    @Override
    public void onBackPressed() {
        if(activityChiamante.equals("InfoConsNoMod")){
            Intent intent=new Intent();
            intent.putExtra(ConsegneItem.RITIRATA,ritirata);
            setResult(RESULT_OK, intent);
            finish();
        }else
            super.onBackPressed();

    }

}