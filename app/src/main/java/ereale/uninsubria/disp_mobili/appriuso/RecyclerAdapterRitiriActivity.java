package ereale.uninsubria.disp_mobili.appriuso;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.Map;
import java.util.TreeMap;

public class RecyclerAdapterRitiriActivity extends RecyclerView.Adapter<RecyclerAdapterRitiriActivity.RitiriItemHolder> implements Filterable {

    private TreeMap<Integer,RitiroItem> mRitiriItems;
    private TreeMap<Integer,RitiroItem> mRitiriItemsFull;

    public RecyclerAdapterRitiriActivity(TreeMap<Integer, RitiroItem> items){mRitiriItems = items;}

    public void updateFullList(TreeMap<Integer, RitiroItem> items){
        mRitiriItemsFull = new TreeMap<>(items);
    }

    // The adapter will work out how many items to display.
    @Override
    public int getItemCount() {
        return mRitiriItems.size();
    }

    // When there are no ViewHolders available. Then RecylerView asks to onCreateViewHolder() to make a new one. The item layout is used to create a view for the ViewHolder.
    @Override
    public RecyclerAdapterRitiriActivity.RitiriItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item_ritiri, parent, false);
        return new RecyclerAdapterRitiriActivity.RitiriItemHolder(inflatedView);
    }

    // Passing in a copy of your ViewHolder and the position where the item will show in your RecyclerView.
    @Override
    public void onBindViewHolder(RecyclerAdapterRitiriActivity.RitiriItemHolder holder, int position) {
        int c=0, key=mRitiriItems.firstKey();
        for(Map.Entry<Integer,RitiroItem> entry : mRitiriItems.entrySet()){
            if(c++==position){
                key = entry.getKey();
                break;
            }
        }
        RitiroItem todoItem = mRitiriItems.get(key);
        holder.bindTodoItem(todoItem);
    }

    public static class RitiriItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {// 1

        private TextView mItemConsegna,mItemCliente,mItemData,mItemOfferta;
        private ImageView mItemImg;
        private int id,idCliente;
        String categoria;

        public RitiriItemHolder(@NonNull View v) {//3
            super(v);
            mItemConsegna = v.findViewById(R.id.item_consegna);
            mItemCliente = v.findViewById(R.id.item_cliente);
            mItemData=v.findViewById(R.id.item_data);
            mItemOfferta=v.findViewById(R.id.item_offerta);
            mItemImg=v.findViewById(R.id.item_imgCategoria);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {//4
            Context context = v.getContext();
            Intent mIntent = new Intent(context, InfoRitiroNoMod.class);
            mIntent.putExtra("chiamante","RitiriActivity");
            mIntent.putExtra(RitiroItem.ID,""+id);
            mIntent.putExtra(RitiroItem.CONSEGNA,mItemConsegna.getText().toString());
            mIntent.putExtra(RitiroItem.CLIENTE, mItemCliente.getText().toString());
            mIntent.putExtra(RitiroItem.IDCLIENTE,idCliente);
            mIntent.putExtra(RitiroItem.DATA,mItemData.getText().toString());
            mIntent.putExtra(RitiroItem.OFFERTA,mItemOfferta.getText().toString());
            mIntent.putExtra(ConsegneItem.CATEGORIA, categoria); //per passare img
            context.startActivity(mIntent);
        }

        public void bindTodoItem(RitiroItem item) {// 5
            leggiConsegne(item.getConsegna(), item);
        }

        private void leggiConsegne(final String cons, final RitiroItem item){
            DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA);
            Query query = myRef.orderByChild(ConsegneItem.CODICE).equalTo(cons);
            query.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    if (dataSnapshot.exists()) {
                        mItemConsegna.setText(item.getConsegna());
                        mItemCliente.setText(item.nomeCliente());
                        mItemData.setText(item.getData());
                        mItemOfferta.setText(""+item.getOfferta());
                        id=item.prendiID();
                        idCliente=item.getCliente();
                        categoria=dataSnapshot.child(ConsegneItem.CATEGORIA).getValue().toString();
                        Categoria.immagineCategoria(mItemImg,categoria);
                    }
                }
                @Override public void onChildChanged(DataSnapshot dataSnapshot, String s) {}
                @Override public void onChildRemoved(DataSnapshot dataSnapshot) {}
                @Override public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
                @Override public void onCancelled(DatabaseError databaseError) {}
            });
        }

    }

    @Override
    public Filter getFilter() { return ritiriItemsFilter; }

    private Filter ritiriItemsFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            TreeMap<Integer, RitiroItem> filteredList = new TreeMap<>();
            if(constraint ==null || constraint.length()==0){
                filteredList.putAll(mRitiriItemsFull);
            }else{
                String filterPattern = constraint.toString().toLowerCase().trim();
                for(Map.Entry<Integer,RitiroItem> entry : mRitiriItemsFull.entrySet()) {
                    int key = entry.getKey();
                    RitiroItem item = entry.getValue();
                    if(item.getConsegna().toLowerCase().contains(filterPattern) || item.nomeCliente().toLowerCase().contains(filterPattern) || item.getData().toLowerCase().contains(filterPattern) || (item.getOfferta()+"").toLowerCase().contains(filterPattern)){
                        filteredList.put(key, item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mRitiriItems.clear();
            mRitiriItems.putAll((TreeMap<Integer, RitiroItem>)results.values);
            notifyDataSetChanged();
        }
    };

}