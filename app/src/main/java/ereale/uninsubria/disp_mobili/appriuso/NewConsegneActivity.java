package ereale.uninsubria.disp_mobili.appriuso;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class NewConsegneActivity extends AppCompatActivity {
    private static final String TAG = "NewConsegneActivity";

    private TreeMap<String, Integer> clienti;
    private RecyclerView mRecyclerView;
    private RecyclerAdapterNewConsegne mAdapter;
    private ArrayList<AdapterConsegneItem> consegneItems;
    private ArrayAdapter<String> adapterCli;
    private AutoCompleteTextView TxtCliente;
    private boolean uscito;
    int idCliente;

    /*calendario*/
    int year_x, month_x, day_x;
    static final int DIALOG_ID=0;
    private TextView data;
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_consegna);
        consegneItems = new ArrayList<>();

        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        uscito = false;

        mRecyclerView = findViewById(R.id.recyclerView);
        mAdapter = new RecyclerAdapterNewConsegne(consegneItems);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAdapter);

        setRecyclerViewItemTouchListener();

        leggiClienti();
        TxtCliente = findViewById(R.id.editTxtCliente);
        adapterCli = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        TxtCliente.setAdapter(adapterCli);

        Button registraCliente = findViewById(R.id.buttonAggiungiCliente);
        registraCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cli = new Intent(NewConsegneActivity.this, NewCliente.class);
                cli.putExtra("chiamante", "NewConsegneActivity");
                startActivityForResult(cli, 2);
            }
        });

        /*calendario*/
        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);
        showDialogOnButtonClick();
        data = findViewById(R.id.txtData);
        String oggi = day_x + "-" + (month_x+1) + "-" + year_x;
        data.setText(oggi);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mIntent = new Intent(NewConsegneActivity.this, NewConsegne.class);
                if (TxtCliente.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Specificare il cliente", Toast.LENGTH_LONG).show();
                } else if (clienti.containsKey(TxtCliente.getText().toString())) {
                    mIntent.putExtra(ConsegneItem.IDCLIENTE, String.valueOf(clienti.get(TxtCliente.getText().toString())));
                    mIntent.putExtra(ConsegneItem.CLIENTE,TxtCliente.getText().toString());
                    mIntent.putExtra(RitiroItem.DATA, data.getText().toString());

                    startActivityForResult(mIntent, 1);
                } else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(NewConsegneActivity.this);
                    alert.setTitle("Nuovo cliente");
                    alert.setMessage("Il cliente " + TxtCliente.getText().toString() + " non è registrato. Vuoi aggiungerlo?");
                    alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent cli = new Intent(NewConsegneActivity.this, NewCliente.class);
                            cli.putExtra("chiamante", "NewConsegneActivity");
                            startActivityForResult(cli, 2);
                        }
                    });
                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                    alert.create().show();
                }

            }
        });

        Button aggiungiConsegna = findViewById(R.id.buttonAggiungiConsegna);
        aggiungiConsegna.setOnClickListener(new View.OnClickListener() {
            @Override
            public synchronized void onClick(View view) {
                if(consegneItems.size()>0) {
                    for (final AdapterConsegneItem itm : consegneItems) {
                        Log.d(TAG,itm+"");

                        final String[] nome=TxtCliente.getText().toString().split(" ");
                        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
                        Log.d(TAG,myRef+"");
                        Query query = myRef.orderByChild(ClienteItem.COGNOME).equalTo(nome[1]);
                        Log.d(TAG,""+query);
                        query.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                for (DataSnapshot d : dataSnapshot.getChildren()) {
                                    Log.d(TAG,""+d);
                                    if (d.child(ClienteItem.NOME).getValue().toString().equals(nome[0])) { //solito problema nome cognome non è univoco
                                        idCliente = Integer.parseInt(d.getKey());
                                        boolean m=false;
                                        if(itm.getMagazzino().equals("true"))
                                            m=true;
                                        ConsegneItem c=new ConsegneItem(idCliente,itm.getDescrizione(),Categoria.getID(itm.getCategoria()),data.getText().toString(),m,itm.getFascia());
                                        c.decidiNomeCliente(TxtCliente.getText().toString());
                                        c.setCliente(Integer.parseInt(d.getKey()));
                                        String[] cod=itm.getConsegna().split("/");
                                        String[] da=data.getText().toString().split("-");
                                        Log.d(TAG, "Codice "+cod[1]+cod[2]);
                                        Log.d(TAG, "Data "+da[1]+da[2]);
                                        if(cod[1].equals(da[1]) && cod[2].equals(da[2])) { //se mese e anno uguali non cambio codice altrimenti si
                                            c.setCodice(itm.getConsegna());
                                        }
                                        Log.d(TAG,itm.getConsegna());
                                        FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + itm.getId()).setValue(c);
                                        Log.d(TAG,"Inserisco: "+c.getCodice());
                                        Log.d(TAG,idCliente+"");
                                        finish();
                                    }
                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {}

                        });
                    }
                    finish();
                }else
                    Toast.makeText(getApplicationContext(), "Nessuna consegna specificata", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1 && resultCode == RESULT_OK) { //dopo aver registrato una consegna
            AdapterConsegneItem rit = new AdapterConsegneItem();
            rit.setConsegna(data.getStringExtra(ConsegneItem.CODICE));
            rit.setDescrizione(data.getStringExtra(ConsegneItem.DESCRIZIONE));
            rit.setFascia(data.getStringExtra(ConsegneItem.FASCIA));
            rit.setMagazzino(data.getStringExtra(ConsegneItem.MAGAZZINO)); //contiene true/false
            rit.setCategoria(data.getStringExtra(ConsegneItem.CATEGORIA));
            rit.setId(data.getStringExtra(ConsegneItem.CONSEGNA));
            consegneItems.add(rit);
            Log.d(TAG,rit+"");
            mAdapter.notifyItemInserted(consegneItems.size());
            TxtCliente.setText(data.getStringExtra(ConsegneItem.CLIENTE));
            this.data.setText(data.getStringExtra(ConsegneItem.DATA));
        }else if(requestCode==2 && resultCode == RESULT_OK){ //dopo aver aggiunto cliente
            String nome = data.getStringExtra(ClienteItem.NOME);
            String cognome = data.getStringExtra(ClienteItem.COGNOME);
            int indice = data.getIntExtra("indice", 1);
            clienti.put(nome+" "+cognome, indice);
            adapterCli.add(nome+" "+cognome);
            Log.d("LOGLOG","ciao"+nome+" "+cognome);
            TxtCliente.setText(nome+" "+cognome);
        }
    }
    private void leggiClienti(){
        clienti = new TreeMap<>();
        DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(ClienteItem.CLIENTE);
        Query query = myRef.orderByChild(ClienteItem.NOME);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    String nc;
                    for(DataSnapshot d : dataSnapshot.getChildren()) {
                        nc="";
                        nc += d.child(ClienteItem.NOME).getValue().toString() + " " + d.child(ClienteItem.COGNOME).getValue().toString();
                        clienti.put(nc, Integer.parseInt(d.getKey()));
                    }
                    adapterCli.addAll(clienti.keySet());
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {}
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == android.R.id.home){
            esc();
        }
        return true;
    }

    @Override public void onBackPressed() {esc();}
    private synchronized void esc(){
        if(consegneItems.size()>0) {
            AlertDialog.Builder alert = new AlertDialog.Builder(NewConsegneActivity.this);
            alert.setTitle("Annullare le consegne?");
            alert.setPositiveButton("Sì", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    for (AdapterConsegneItem itm : consegneItems) {
                        FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + itm.getId()).removeValue();
                    }
                    uscito=true;
                    finish();
                }
            });
            alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override public void onClick(DialogInterface dialog, int which) {}
            });
            alert.create().show();
        }else {
            uscito=true;
            finish();
        }
    }

    private void setRecyclerViewItemTouchListener() {
        // 1 - You create the callback and tell it what events to listen for.
        // It takes two parameters, one for drag directions and one for swipe directions, but
        // you’re only interested in swipe, so you pass 0 to inform the callback not to respond to drag events.
        ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT)
        {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder1) {
                // 2 - You return false in onMove because you don’t want to perform any special behavior here.
                return false;
            }
            @Override
            public synchronized void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                // 3 - onSwiped is called when you swipe an item in the direction specified in the
                // ItemTouchHelper. Here, you request the viewHolder parameter passed for the
                // position of the item view, then you remove that item from your list of photos.
                // Finally, you inform the RecyclerView adapter that an item has been removed at a
                // specific position.
                int position = viewHolder.getAdapterPosition();
                AdapterConsegneItem item = consegneItems.remove(position);
                // delete the item from the DB
                FirebaseDatabase.getInstance().getReference(ConsegneItem.CONSEGNA + "/" + item.getId()).removeValue(); //perchè lo inserisco in NewConsegne
                mRecyclerView.getAdapter().notifyItemRemoved(position);
            }
        };
        // 4 - You initialize the ItemTouchHelper with the callback behavior you defined, and
        // then attach it to the RecyclerView.
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    /*Per calendario*/
    public void showDialogOnButtonClick(){
        btn=(Button) findViewById(R.id.buttonCalendario);
        btn.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        showDialog(DIALOG_ID);
                    }
                }
        );
    }
    @Override
    protected Dialog onCreateDialog(int id){
        if(id == DIALOG_ID)
            return new DatePickerDialog(this, dpickerListener, year_x, month_x, day_x);
        return null;
    }
    private DatePickerDialog.OnDateSetListener dpickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            year_x = year;
            month_x = month+1;
            day_x = dayOfMonth;
            data.setText(day_x+"-"+month_x+"-"+year_x);
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) { //per salvataggio dati quando il telefono gira
        super.onSaveInstanceState(outState);
        outState.putBoolean("salvato",!uscito);
        if(!uscito) {
            outState.putInt("num", consegneItems.size());
            int i = 0;
            for (AdapterConsegneItem r : consegneItems) {
                outState.putString(ConsegneItem.CONSEGNA + "" + i, r.getId());
                outState.putString(ConsegneItem.DESCRIZIONE + "" + i, r.getDescrizione());
                outState.putString(ConsegneItem.FASCIA + "" + i, r.getFascia());
                outState.putString(ConsegneItem.CATEGORIA + "" + i, r.getCategoria());
                outState.putString(ConsegneItem.MAGAZZINO+""+i,r.getMagazzino());
                outState.putString(ConsegneItem.CODICE+ "" + i, r.getConsegna());
                outState.putString("img" + i, r.getIdImg());
                i++;
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) { //per salvataggio dati quando il telefono gira
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState.getBoolean("salvato")){
            int num = savedInstanceState.getInt("num");
            for(int i=0;i<num;i++){
                AdapterConsegneItem ad = new AdapterConsegneItem();
                ad.setCategoria(savedInstanceState.getString(ConsegneItem.CATEGORIA+""+i));
                ad.setId(savedInstanceState.getString(ConsegneItem.CONSEGNA+""+i));
                ad.setConsegna(savedInstanceState.getString(ConsegneItem.CODICE+""+i));
                ad.setDescrizione(savedInstanceState.getString(ConsegneItem.DESCRIZIONE+""+i));
                ad.setFascia(savedInstanceState.getString(ConsegneItem.FASCIA+""+i));
                ad.setMagazzino(savedInstanceState.getString(ConsegneItem.MAGAZZINO+i));
                consegneItems.add(ad);
            }
        }
    }

}
